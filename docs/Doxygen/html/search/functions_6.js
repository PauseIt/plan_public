var searchData=
[
  ['getfaqdata',['GetFaqData',['../class_blazor_secure_login_1_1_server_1_1_controllers_1_1_faq_data_controller.html#ae1dd45b3ee58925c46361ed57eccab60',1,'BlazorSecureLogin::Server::Controllers::FaqDataController']]],
  ['getinfo',['GetInfo',['../class_blazor_secure_login_1_1_server_1_1_controllers_1_1_user_controller.html#a9aa214cb3e34151ace283fcac2ba9bf4',1,'BlazorSecureLogin::Server::Controllers::UserController']]],
  ['getuserinfo',['GetUserInfo',['../interface_blazor_secure_login_1_1_client_1_1_services_1_1_contracts_1_1_i_authorize_api.html#a5004badb58d199afb855b4bb8f04af5c',1,'BlazorSecureLogin.Client.Services.Contracts.IAuthorizeApi.GetUserInfo()'],['../class_blazor_secure_login_1_1_client_1_1_services_1_1_implementations_1_1_authorize_api.html#afed768f43c0a7f595c068b4abd5b4689',1,'BlazorSecureLogin.Client.Services.Implementations.AuthorizeApi.GetUserInfo()'],['../class_blazor_secure_login_1_1_client_1_1_states_1_1_authentication_state.html#acf80cbb529cb547293ef22268c828626',1,'BlazorSecureLogin.Client.States.AuthenticationState.GetUserInfo()']]]
];
