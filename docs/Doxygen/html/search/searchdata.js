var indexSectionsWithContent =
{
  0: "2_abcdefgilmnopqrstuv",
  1: "_acdefilmnprsuv",
  2: "b",
  3: "2_abcdefilmnprsuv",
  4: "abcdefgilmoprsu",
  5: "i",
  6: "abcdefglnopqrstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};

