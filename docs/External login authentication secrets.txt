External authentication guide

https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/?view=aspnetcore-2.2&tabs=visual-studio

execute commands in powershell from the server folder e.g. c:/Users/<CurrentUser>/tec_repo/BlazorSecureLogin/src/BlazorSecureLogin.Server/
This will append the secrets.json file that will contain the appids & secrets for the external login providers on your local machine.

Facebook authentication
dotnet user-secrets set Authentication:Facebook:AppId 2299504350367831
dotnet user-secrets set Authentication:Facebook:AppSecret 322332dd12f9ec9c74da54c1eb8540a5


Google authentication

dotnet user-secrets set "Authentication:Google:ClientId" "328811842375-459e3goob74ndsul04jmmkn2bq32dftf.apps.googleusercontent.com"
dotnet user-secrets set "Authentication:Google:ClientSecret" "wS1rAEVRECpu7hlV_R2Xu9au"

Microsoft Authentication

dotnet user-secrets set "Authentication:MicrosoftAccount:ClientId" "3c88bb20-802b-4f44-baf9-b556bfb8e75c"
dotnet user-secrets set "Authentication:MicrosoftAccount:ClientSecret" "SW8zFyh:110Cp+MpWVMz.c*ldnW=]CJh"

Twitter Authentication

dotnet user-secrets set "Authentication:Twitter:ConsumerAPIKey" "XeIVYD6s82lfZrk5c3P41PznG"
dotnet user-secrets set "Authentication:Twitter:ConsumerSecret" "XwguynhRz9plU3gx41OUrlfRl2xFBCaSrtIUURj4tZ0sYjAMFm"
