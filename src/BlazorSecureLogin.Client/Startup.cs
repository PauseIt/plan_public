/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: MatBlazor
using BlazorSecureLogin.Client.Services.Contracts;
using BlazorSecureLogin.Client.Services.Implementations;
using BlazorSecureLogin.Client.States;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using MatBlazor;
using Toolbelt.Blazor.Extensions.DependencyInjection;

namespace BlazorSecureLogin.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // This will setup the notification popup, MatToaster, so it's callable from the razor pages.
            // The toaster card will show in the container defined in App.razor.
            services.AddMatToaster(config =>
            {
                config.Position = MatToastPosition.BottomRight;
                config.PreventDuplicates = true;
                config.NewestOnTop = true;
                config.ShowCloseButton = true;
                config.MaximumOpacity = 95;
                config.VisibleStateDuration = 5000;
            });

            // Configures dependency injection
            services.AddScoped<UserDataState>();
            services.AddScoped<EntityDataState>();
            services.AddScoped<OrganizationDataState>();
            services.AddScoped<GroupDataState>();
            services.AddScoped<AuthenticationState>();
            services.AddScoped<IAuthorizeApi, AuthorizeApi>();
            // adds loading bar when loading pages.
            services.AddLoadingBar();

            
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.UseLoadingBar();
            app.AddComponent<App>("app");
        }
    }
}
