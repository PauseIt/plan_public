﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// This class is used to cache entity specific information and client handles to call the API
    /// </summary>
    public class EntityDataState : BaseState
    {
        
        public EntityDataState(HttpClient httpClient) : base(httpClient) { }
        private EntityParameters _EntityParameters { get; set; } = null;
        public EntityParameters EntityParameters => this._EntityParameters;

        private List<GroupParameters> _EntityGroups { get; set; } = null;
        public List<GroupParameters> EntityGroups => this._EntityGroups != null ? this._EntityGroups : this._EntityGroups = GetEntityGroups().Result;

        private OrganizationParameters _EntityOrganization { get; set; } = null;
        public OrganizationParameters EntityOrganization => this._EntityOrganization != null ? this._EntityOrganization : this._EntityOrganization = GetEntityOrganization().Result;

        private async Task<OrganizationParameters> GetEntityOrganization()
        {
            this.InitializeCheck();
            return await GetObjectFromId<OrganizationParameters>("api/Entity/GetEntityOrganization", this.EntityParameters.Entity.Id);
        }
        
        public async Task ChangeEntity(long id)
        {
            this._EntityParameters = await GetObjectFromId<EntityParameters>("api/Entity/SetActiveEntity", id);
            NotifyOnLogin();
            NotifyStateChanged();
        }

        public async Task<List<GroupParameters>> GetEntityGroups()
        {
            this.InitializeCheck();
            return await GetObjectFromId<List<GroupParameters>>("api/Entity/GetGroupsAsAdmin", this.EntityParameters.Entity.Id);
        }

        public async Task<EntityParameters> UpdateEntityParameters(EntityParameters entityParameters)
        {
            this.InitializeCheck();
            return this._EntityParameters = await PostObject<EntityParameters>("api/Entity/UpdateEntityParamerters", entityParameters);
        }

        private void InitializeCheck()
        {
            if (this._EntityParameters == null)
            {
                NotifyError();
            }
        }

        public void Clear()
        {
            this._EntityParameters = null;
            this._EntityGroups = null;
            this._EntityOrganization = null;
            this.NotifyOnClear();
        }
    }
}
