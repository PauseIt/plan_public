﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BlazorSecureLogin.Shared;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// Contract for the BaseState class
    /// </summary>
    public interface IBaseState
    {
        Task<T> GetObject<T>(string uri) where T : BaseModel;
        Task<T> GetObjects<T>(string uri) where T : List<BaseModel>;
        Task<T> GetObjectFromId<T>(string uri, long id) where T : BaseModel;
        Task<T> GetObjectsFromId<T>(string uri, long id) where T : List<BaseModel>;
        Task<T> PatchObject<T>(string uri, T obj) where T : BaseModel;
        Task<T> PatchObjects<T>(string uri, T obj) where T : List<BaseModel>;
        Task<T> PostObject<T>(string uri, T obj) where T : BaseModel;
        Task<T> PostObjects<T>(string uri, T obj) where T : List<BaseModel>;
        Task<T> PutObject<T>(string uri, T obj) where T : BaseModel;
        Task<T> PutObjects<T>(string uri, T obj) where T : List<BaseModel>;
        Task<T> DeleteObject<T>(string uri, T obj) where T : BaseModel;
        Task<T> DeleteObjects<T>(string uri, T obj) where T : List<BaseModel>;
    }
}