﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// Class with generic helper functions to call the API and events that can be raised to notify razor pages to re-render.
    /// This class should be inherited in all DataState classes as it provides default functionality for all states.
    /// </summary>
    public class BaseState
    {
        public readonly HttpClient _httpClient;

        public BaseState(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public event Action OnCreate;
        protected void NotifyOnCreate() => OnCreate?.Invoke();
        public event Action OnLogin;
        protected void NotifyOnLogin() => OnLogin?.Invoke();
        public event Action OnClear;
        protected void NotifyOnClear() => OnClear?.Invoke();
        public event Action OnChange;
        protected void NotifyStateChanged() => OnChange?.Invoke();
        public event Action OnError;
        protected void NotifyError() => OnError?.Invoke();

        /// <summary>
        /// Method to handle the Http response message from the server.
        /// Will handle error messages for the client.
        /// </summary>
        /// <param name="httpResponse"></param>
        /// <returns></returns>
        private async Task<string> HandleResponseMessage(HttpResponseMessage httpResponse)
        {            
            if (httpResponse.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new Exception(await httpResponse.Content.ReadAsStringAsync());
            }

            // Implement response handler for more explicid statuscodes... 418 : I'm a teapot!
            httpResponse.EnsureSuccessStatusCode();
            return await httpResponse.Content.ReadAsStringAsync();
        }

        private T DeserializeHandler<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when deserializing object:");
                Console.WriteLine(e);
                throw;
            } finally
            {
                NotifyStateChanged();
            }
        }
        
        /// <summary>
        /// GET
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<T> GetObject<T>(string uri)
        {
            var data = await HandleResponseMessage(await _httpClient.GetAsync(uri));
            return DeserializeHandler<T>(data);
        }
        
        /// <summary>
        /// GET (with params)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetObjectFromId<T>(string uri, long id)
        {
            return await this.GetObject<T>(uri + "?Id=" + id);
        }
        
        /// <summary>
        /// POST
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<T> PostObject<T>(string uri, object obj)
        {
            try
            {
                var data = await _httpClient.PostJsonAsync<T>(uri, obj);
                if (data == null)
                {
                    throw new Exception("server did not return valid data.");
                }
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine("PostJsonAsync failed with: " + e);
                throw e;
            } finally
            {
                NotifyStateChanged();
            }
        }

        /// <summary>
        /// PATCH / UPDATE
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<T> PatchObject<T>(string uri, object obj)
        {
            return await PostObject<T>(uri, obj);
        }

        /// <summary>
        /// PUT
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<T> PutObject<T>(string uri, object obj)
        {
            return await PostObject<T>(uri, obj);
        }

        /// <summary>
        /// DELETE
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<bool> DeleteObject(string uri)
        {
            var data = await HandleResponseMessage(await _httpClient.DeleteAsync(uri));
            return DeserializeHandler<bool>(data);
        }

        public async Task<bool> DeleteObjectFromId(string uri, long id)
        {
            return await this.DeleteObject(uri + "?Id=" + id);
        }
    }
}
