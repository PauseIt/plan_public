﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using MatBlazor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// This class is used to cache group specific information and client handles to call the API
    /// </summary>
    public class GroupDataState : BaseState
    {
        private GroupParameters _groupParamerters { get; set; } = new GroupParameters();
        public GroupParameters groupParamerters => this._groupParamerters;


        public async Task<List<CalendarData>> GetCalendarDatas()
        {
            CalendarParameters calendarParameters = new CalendarParameters();
            calendarParameters.CalendarType = CalendarType.Group;
            calendarParameters.Id = this._groupParamerters.Group.Id;
            var data = await PostObject<List<CalendarData>>("api/Calendar/GetCalendarDatas", calendarParameters);
            return data;
        }

        private List<Entity> _GroupEntities { get; set; } = null;
        public async Task<List<Entity>> GetGroupEntities()
        {
            Console.WriteLine("GetGroupEntities was called");
            if (this._GroupEntities != null)
            {
                return this._GroupEntities;
            }

            return this._GroupEntities = await LoadGroupEntities();
        }

        public async Task<List<Entity>> LoadGroupEntities()
        {
            Console.WriteLine("LoadGroupEntities was called");
            return await GetObjectFromId<List<Entity>>("/api/Group/GetGroupEntities", this._groupParamerters.Group.Id);
        }

        public GroupDataState(HttpClient httpClient) : base(httpClient) { }
        public async Task ChangeGroup(long id)
        {
            this._groupParamerters = await PostObject<GroupParameters>("api/Group/SetActiveGroup", id);
            this._GroupEntities = null;
            NotifyOnLogin();
            NotifyStateChanged();
        }

        public async Task CheckActiveGroup( Group group )
        {

            // Check if the group is the one loaded inside the data state. If not. call API and load new group...

            //this._group = await PostObject<Group>("api/Organization/SetActiveGroup", id);
            //this._Group_Id = this.group.Id;
            Console.WriteLine("CheckActiveGroup was called");
            NotifyStateChanged();
        }

        public async Task UpdateGroupParameters(GroupParameters groupParameters)
        {
            // TODO
            // create endpoint for posting data and setting new params.
        }

        public void Clear()
        {
            this._groupParamerters = null;
            this.NotifyOnClear();
        }
    }
}
