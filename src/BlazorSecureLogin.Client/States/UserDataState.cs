﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;   
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net.Http;
using BlazorSecureLogin.Shared;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// This class is used to cache end user specific information and handles to call the API
    /// </summary>
    public class UserDataState : BaseState
    {
        public UserDataState(HttpClient httpClient) : base(httpClient) { }

        private List<Organization> _UserOrganizations { get; set; }
        public async Task<List<Organization>> GetUserOrganizations()
        {
            if (this._UserOrganizations != null) {
                return this._UserOrganizations.OrderBy(x => x.Title).ToList();
            }

            this._UserOrganizations = await GetObject<List<Organization>>("api/User/GetUserOrganizations");
            return this._UserOrganizations.OrderBy(x => x.Title).ToList();
        }

        private List<Entity> _UserEntities { get; set; }
        public async Task<List<Entity>> GetUserEntities()
        {
            if (this._UserEntities != null)
            {
                return this._UserEntities.OrderBy(x => x.Name).ToList();
            }
            
            this._UserEntities = await GetObject<List<Entity>>("api/User/GetUserEntities");
            return this._UserEntities.OrderBy(x => x.Name).ToList();
        }

        public async Task<List<CalendarData>> GetCalendarDatas()
        {
            CalendarParameters calendarParameters = new CalendarParameters();
            calendarParameters.CalendarType = CalendarType.User;
            var data = await PostObject<List<CalendarData>>("api/Calendar/GetCalendarDatas", calendarParameters);
            return data;
        }

        public async Task<OrganizationParameters> AddOrganization(OrganizationParameters organizationParameters)
        {
            var data = await PostObject<OrganizationParameters>("api/Organization/AddOrganization", organizationParameters);
            this._UserOrganizations = null;
            this.NotifyOnCreate();
            return data;
        }

        public void updateParameters(OrganizationParameters organizationParameters)
        {
            if (this._UserOrganizations == null)
            {
                this._UserOrganizations = new List<Organization>();
            }

            var index = this._UserOrganizations.FindIndex(x => x.Id == organizationParameters.Organization.Id);

            if (index <= 0)
            {
                this._UserOrganizations.Add(organizationParameters.Organization);
            }
            else
            {
                this._UserOrganizations[index] = organizationParameters.Organization;
            }

            NotifyOnCreate();
        }

        public void updateParameters(EntityParameters entityParameters)
        {
            if (this._UserEntities == null)
            {
                this._UserEntities = new List<Entity>();
            }

            var index = this._UserEntities.FindIndex(x => x.Id == entityParameters.Entity.Id);

            if (index <= 0)
            {
                this._UserEntities.Add(entityParameters.Entity);
            }
            else
            {
                this._UserEntities[index] = entityParameters.Entity;
            }

            NotifyOnCreate();
        }

        public void Clear()
        {
            this._UserOrganizations = null;
            this._UserEntities = null;
        }
    }
}
