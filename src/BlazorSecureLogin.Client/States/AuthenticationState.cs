﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing Platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: Microsoft ASP.NET Identity best pratice
/// File description: This file is the wrapper class that is injected in all authorized pages.
/// This logic is to avoid exposing all the actual code for the API calls.
using BlazorSecureLogin.Client.Services.Contracts;
using BlazorSecureLogin.Shared;
using System.Net.Http;
using System.Threading.Tasks;
using BlazorSecureLogin.Shared.DTO;

namespace BlazorSecureLogin.Client.States
{
    public class AuthenticationState
    {
        private readonly IAuthorizeApi _authorizeApi;
        public AuthenticationState(IAuthorizeApi authorizeApi)
        {
            _authorizeApi = authorizeApi;
        }

        /// <summary>
        /// Check is the user is logged in by asking the API controller to call server and ask for active session with valid user information.
        /// </summary>
        /// <returns>boolean to determine if user is logged in or not</returns>
        public async Task<bool> IsLoggedIn()
        {
            try
            {
                var userInfo = await GetUserInfo();
                return userInfo != null;
            }
            catch (HttpRequestException)
            {
                return false;
            }
        }

        /// <summary>
        /// Ask the API controller to send authentication parameters to server to attempt a login from login page.
        /// Save the user information in user info field on sucess
        /// </summary>
        /// <param name="loginParameters">The form parameters from frontend</param>
        public async Task Login(LoginParameters loginParameters)
        {
            await _authorizeApi.Login(loginParameters);
        }

        /// <summary>
        /// Ask the API controller to register a new lacally stored user using the form parameters from the UI
        /// </summary>
        /// <param name="registerParameters">UI from params</param>
        /// <returns></returns>
        public async Task Register(RegisterParameters registerParameters)
        {
            await _authorizeApi.Register(registerParameters);
        }

        /// <summary>
        /// Ask the API controller to logout user from both client and server
        /// </summary>
        public async Task Logout()
        {
            await _authorizeApi.Logout();
        }

        /// <summary>
        /// Ask the API controller for the user information on the currently authorized user.
        /// </summary>
        /// <returns>A user information object</returns>
        public async Task<UserInfo> GetUserInfo()
        {
            return await _authorizeApi.GetUserInfo();
        }
        /// <summary>
        /// Ask the API controller the update the user information provided by the frontend UI form from edit user.
        /// </summary>
        /// <param name="info">the new user information object that is provided from frontend</param>
        /// <returns>A new and updated user information object upon success</returns>
        public async Task<UserInfo> UpdateUserInfo(UserInfo info)
        {
            return await _authorizeApi.UpdateUserInfo(info);
        }

        
    }
}
