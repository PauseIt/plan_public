﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using BlazorSecureLogin.Shared.DTO.Visualizations;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BlazorSecureLogin.Client.States
{
    /// <summary>
    /// This class is used to cache organization specific information and client handles to call the API
    /// </summary>
    public class OrganizationDataState : BaseState
    {
        public OrganizationDataState(HttpClient httpClient) : base(httpClient) { }

        private bool? _IsOrganizationAdmin { get; set; } = null;
        public async Task<bool> GetIsOrganizationAdmin()
        {
            if (this._IsOrganizationAdmin == null)
            {
                this._IsOrganizationAdmin = await GetObjectFromId<bool>("/api/Organization/OrganizationOwner", this.organizationParamerters.Organization.Id);
            }

            return this._IsOrganizationAdmin.Value;
        }

        private bool? _IsGroupAdmin { get; set; } = null;
        public async Task<bool> GetIsGroupAdmin()
        {
            if (this._IsGroupAdmin == null)
            {
                this._IsGroupAdmin = await GetObjectFromId<bool>("/api/Organization/GroupOwner", this.organizationParamerters.Organization.Id);
            }

            return this._IsGroupAdmin.Value;
        }

        private List<GroupVisualization> _AllOrganizationGroupVisualizations { get; set; } = null;
        public async Task<List<GroupVisualization>> GetAllOrganizationGroupVisualizations()
        {
            if (this._AllOrganizationGroupVisualizations != null)
            {
                return _AllOrganizationGroupVisualizations;
            }
            return this._AllOrganizationGroupVisualizations = await LoadAllGroupVisualizations();
        }

        private List<EntityParameters> _OrganizationEntities { get; set; } = null;
        public async Task<List<EntityParameters>> GetOrganizationEntities()
        {
            if (this._OrganizationEntities != null)
            {
                return this._OrganizationEntities;
            }
            return this._OrganizationEntities = await LoadOrganizationEntities();
        }

        private List<EntityVisualization> _AllOrganizationEntityVisualizations { get; set; } = null;
        public async Task<List<EntityVisualization>> GetAllOrganizationEntityVisualizations()
        {
            if (this._AllOrganizationEntityVisualizations != null)
            {
                return _AllOrganizationEntityVisualizations;
            }
            return this._AllOrganizationEntityVisualizations = await LoadAllEntityVisualizations();
        }

        private List<GroupParameters> _GroupsAsAdmin { get; set; } = null;
        public async Task<List<GroupParameters>> GetGroupsAsAdmin()
        {
            if (this._GroupsAsAdmin != null)
            {
                return _GroupsAsAdmin;
            }
            return this._GroupsAsAdmin = await LoadGroupsAsAdmin();
        }

        private List<GroupParameters> _AllGroups { get; set; } = null;
        public async Task<List<GroupParameters>> GetAllGroups()
        {
            if (this._AllGroups != null)
            {
                return _AllGroups;
            }
            return this._AllGroups = await LoadAllGroups();
        }
        private OrganizationParameters _organizationParamerters { get; set; } = new OrganizationParameters();
        public OrganizationParameters organizationParamerters => this._organizationParamerters;

        public async Task ChangeOrganization(long id)
        {
            this._IsGroupAdmin = null;
            this._IsOrganizationAdmin = null;
            this._AllOrganizationGroupVisualizations = null;
            this._GroupsAsAdmin = null;
            this._organizationParamerters = await PostObject<OrganizationParameters>("api/Organization/SetActiveOrganization", id);
            NotifyOnLogin();
            NotifyStateChanged();
        }

        public async Task<List<CalendarData>> GetCalendarDatas()
        {
            CalendarParameters calendarParameters = new CalendarParameters();
            calendarParameters.CalendarType = CalendarType.Organization;
            calendarParameters.Id = this.organizationParamerters.Organization.Id;
            var data = await PostObject<List<CalendarData>>("api/Calendar/GetCalendarDatas", calendarParameters);
            return data;
        }

        public async Task<List<GroupParameters>> LoadGroupsAsAdmin()
        {
            if (!(await this.GetIsGroupAdmin()))
            {
                return new List<GroupParameters>();
            }

            return await GetObjectFromId<List<GroupParameters>>("api/Organization/GetGroupsAsAdmin", this.organizationParamerters.Organization.Id);
        }

        public async Task<List<GroupParameters>> LoadAllGroups()
        {
            if (!(await this.GetIsGroupAdmin()))
            {
                return new List<GroupParameters>();
            }

            return await GetObjectFromId<List<GroupParameters>>("api/Organization/GetAllGroups", this.organizationParamerters.Organization.Id);
        }

        private async Task<List<GroupVisualization>> LoadAllGroupVisualizations()
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return new List<GroupVisualization>();
            }

            return await PostObject<List<GroupVisualization>>("/api/Organization/GetAllGroupVisualizations", this._organizationParamerters);
        }

        private async Task<List<EntityVisualization>> LoadAllEntityVisualizations()
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return new List<EntityVisualization>();
            }
            return await PostObject<List<EntityVisualization>>("/api/Organization/GetAllEntityVisualizations", this._organizationParamerters);
        }

        private async Task<List<EntityParameters>> LoadOrganizationEntities()
        {
            
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return null;
            }
            
            return await PostObject<List<EntityParameters>>("/api/Organization/GetOrganizationEntities", this._organizationParamerters);
        }

        public async Task<OrganizationParameters> UpdateOrganizationParameters( OrganizationParameters organizationParameters )
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return null;
            }

            var data = await PostObject<OrganizationParameters>("/api/Organization/UpdateOrganization", organizationParameters);
            this._organizationParamerters = data;
            this.NotifyStateChanged();
            return data;
        }

        public async Task<GroupParameters> CreateGroup (GroupParameters groupParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return null;
            }

            var data = await PostObject<GroupParameters>("/api/Organization/CreateGroup", groupParameters);
            this._AllOrganizationGroupVisualizations = null;
            return data;
        }

        public async Task<GroupParameters> UpdateGroup (GroupParameters groupParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return null;
            }

            return await PostObject<GroupParameters>("/api/Organization/UpdateGroup", groupParameters);
        }

        public async Task<bool> DeleteGroup (long id)
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return false;
            }
            var data = await DeleteObjectFromId("/api/Organization/DeleteGroup", id);
            this._AllOrganizationGroupVisualizations = null;
            this._GroupsAsAdmin = null;
            this.NotifyOnCreate();
            return data;
        }

        public async Task<bool> GroupInviteUser (UserInviteParameters userInviteParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()))
            {
                return false;
            }

            var data = await PostObject<bool>("/api/Organization/GroupInviteUser", userInviteParameters);
            this._AllOrganizationGroupVisualizations = null;
            this._GroupsAsAdmin = null;
            this._IsGroupAdmin = null;
            this.NotifyStateChanged();
            this.NotifyOnCreate();
            return data;
        }

        public async Task<EntityParameters> CreateEntity(EntityParameters entityParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return null;
            }
            var data = await PostObject<EntityParameters>("/api/Organization/CreateEntity", entityParameters);
            if (data != null)
            {
                this._OrganizationEntities = null;
                this._AllOrganizationEntityVisualizations = null;
            }
            return data;
        }

        public async Task<EntityParameters> UpdateEntity(EntityParameters entityParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return null;
            }

            return await PostObject<EntityParameters>("/api/Organization/UpdateEntity", entityParameters);
        }

        public async Task<bool> DeleteEntity(long id)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return false;
            }

            var data = await DeleteObjectFromId("/api/Organization/DeleteEntity", id);
            this._AllOrganizationEntityVisualizations = null;
            return data;
        }

        public async Task<bool> EntityInviteUser(UserInviteParameters userInviteParameters)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return false;
            }

            var data = await PostObject<bool>("/api/Organization/EntityInviteUser", userInviteParameters);
            this._AllOrganizationEntityVisualizations = null;
            this.NotifyStateChanged();
            return data;
        }

        public async Task<List<long>> GetEntityIdsFromGroupId(long groupId)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return new List<long>();
            }

            var data = await GetObjectFromId<List<long>>("/api/Organization/GetEntityIdsFromGroupId", groupId);
            this.NotifyStateChanged();
            return data;
        }

        public async Task<bool> LinkEntityAndGroup (Entity_Group entity_Group)
        {
            if (!(await this.GetIsOrganizationAdmin()) || !(await this.GetIsGroupAdmin()))
            {
                return false;
            }

            var data = await PostObject<bool>("/api/Organization/LinkEntityAndGroup", entity_Group);
            this._AllOrganizationEntityVisualizations = null;
            this.NotifyStateChanged();
            return data;
        }

        public async Task<bool> AddCalendarData(CalendarPutParameters calendarPutParameters)
        {
            if(!(await this.GetIsOrganizationAdmin()))
            {
                return false;
            }

            var data = await PostObject<bool>("/api/Calendar/CreateCalendarData", calendarPutParameters);
            this.NotifyStateChanged();
            return data;
        }

        public void Clear()
        {
            this._AllGroups = null;
            this._AllOrganizationEntityVisualizations= null;
            this._AllOrganizationGroupVisualizations = null;
            this._GroupsAsAdmin = null;
            this._IsOrganizationAdmin = null;
            this._IsGroupAdmin = null;
            this._organizationParamerters = new OrganizationParameters();
            this.NotifyOnClear();
        }
    }    
}
