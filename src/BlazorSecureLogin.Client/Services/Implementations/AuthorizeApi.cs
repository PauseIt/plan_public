﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing Platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: ASP.NET Identity Framework
/// File description: This file is for handeling client side user authorization
using BlazorSecureLogin.Client.Services.Contracts;
using BlazorSecureLogin.Shared;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.Services.Implementations
{
    public class AuthorizeApi : IAuthorizeApi
    {
        private readonly HttpClient _httpClient;

        public AuthorizeApi(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        /// <summary>
        /// This method is an ansyc operation that calls the Blazor.Server API and authenticates the user.
        /// </summary>
        /// <param name="loginParameters"></param>
        /// <returns>
        /// User information for the authenticated user.
        /// </returns>
        public async Task<UserInfo> Login(LoginParameters loginParameters)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(loginParameters), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Authorize/Login", stringContent);
            if (result.StatusCode == System.Net.HttpStatusCode.BadRequest) throw new Exception(await result.Content.ReadAsStringAsync());
            result.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfo>(await result.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// This method is an ansyc operation that calls the Blazor.Server API and logs the user out (The API will redirect to landing page for unauthorized users) 
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            var result = await _httpClient.PostAsync("api/Authorize/Logout", null);
            result.EnsureSuccessStatusCode();
        }

        /// <summary>
        /// This method is an ansyc operation that calls the Blazor.Server API and creates a new user with the informatino stored in the RegisterParameters object.
        /// </summary>
        /// <param name="registerParameters"></param>
        /// <returns>
        /// User information for the created & authenticated user.
        /// </returns>
        public async Task<UserInfo> Register(RegisterParameters registerParameters)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(registerParameters), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/Authorize/Register", stringContent);
            if (result.StatusCode == System.Net.HttpStatusCode.BadRequest) throw new Exception(await result.Content.ReadAsStringAsync());
            result.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfo>(await result.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// This method is an ansyc operation that calls the Blazor.Server API and gets the user information of the currently authenticated user using HttpContext (session).
        /// </summary>
        /// User information for the authenticated user.
        /// <returns></returns>
        public async Task<UserInfo> GetUserInfo()
        {
            var result = await _httpClient.GetJsonAsync<UserInfo>("api/User/GetInfo");
            return result;
        }

        /// <summary>
        /// This method is an ansyc operation that calls the Blazor.Server API and updates the user in the database matching the user provided in the UserInfo parameter
        /// </summary>
        /// <param name="info"></param>
        /// New user information of the updated user.
        /// <returns></returns>
        public async Task<UserInfo> UpdateUserInfo(UserInfo info)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(info), Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync("api/user/PostInfo", stringContent);
            if (result.StatusCode == System.Net.HttpStatusCode.BadRequest) throw new Exception(await result.Content.ReadAsStringAsync());
            result.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfo>(await result.Content.ReadAsStringAsync());
        }
    }
}
