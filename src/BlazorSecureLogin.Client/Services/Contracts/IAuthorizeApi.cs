﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing Platform
/// Author: Martin Puge
/// Editor: N/A
/// External credits: ASP.NET Identity framework
using BlazorSecureLogin.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.Services.Contracts
{
    /// <summary>
    /// Interface for Client side Authorize Api
    /// Implemented in AuthorizeApi.cs
    /// </summary>
    public interface IAuthorizeApi
    {
        Task<UserInfo> Login(LoginParameters loginParameters);
        Task<UserInfo> Register(RegisterParameters registerParameters);
        Task Logout();
        Task<UserInfo> GetUserInfo();
        Task<UserInfo> UpdateUserInfo(UserInfo info);
    }
}
