﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Linq;
using BlazorSecureLogin.Shared;
using ServiceStack.OrmLite;
using BlazorSecureLogin.Server.Services.Contracts;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class GroupHelper : BaseHelper, IGroupHelper
    {
        public GroupHelper(IUserHelper userHelper) : base(userHelper) { }

        // Holds current groupParameters
        private GroupParameters _GroupParameters { get; set; } = null;
        public GroupParameters GroupParameters
        {
            get => this._GroupParameters;
        }

        private List<Entity> _GroupEntities { get; set; } = null;
        public List<Entity> GroupEntities
        {
            get => (_GroupEntities != null) ? _GroupEntities : _GroupEntities = LoadGroupEntities();
        }

        /// <summary>
        /// Gets the entities in the current groupId.
        /// </summary>
        /// <returns></returns>
        private List<Entity> LoadGroupEntities()
        {
            if (!this.IsInitialized() || !this._GroupOwnerBy(this.GroupParameters.Group.Id))
            {
                return null;
            }

            List<Entity> output = new List<Entity>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entityQuery = Db.From<Entity>()
                    .Join<Entity_Group>((e, eg) => e.Id == eg.Entity_Id)
                    .Where<Entity_Group>(x => x.Group_Id == this.GroupParameters.Group.Id);

                output = Db.Select(entityQuery);
            });

            return output;
        }

        /// <summary>
        /// Validates if current class has been initialized
        /// </summary>
        /// <returns></returns>
        private bool IsInitialized()
        {
            return this._GroupParameters != null;
        }

        /// <summary>
        /// Method to change current working group.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task SetGroupParameters(long id)
        {
            this._GroupParameters = null;
            this._GroupEntities = null;
            if (!this._GroupOwnerBy(id))
            {
                return;
            }

            this._GroupParameters = new GroupParameters();

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                try
                {
                    this._GroupParameters.Group = Db.Single(
                        Db.From<Group>()
                        .Where(x => x.Id == id));
                }
                catch (Exception e)
                {
                    throw (e);
                }
            });
        }

        /// <summary>
        /// Gets current working group.
        /// </summary>
        /// <returns></returns>
        public GroupParameters LoadGroupParameters()
        {
            if (!this.IsInitialized())
            {
                return null;
            }
            return this.GroupParameters;
        }

        /// <summary>
        /// Creates a new group record in the database
        /// </summary>
        /// <param name="groupParameters"></param>
        /// <returns></returns>
        public async Task<bool> CreateGroup(GroupParameters groupParameters)
        {
            long newGroupId = 0;
            if (!this._GroupOwnerBy(groupParameters.Group.Id))
            {
                throw new InvalidOperationException();
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                newGroupId = Db.Insert(groupParameters.Group, true);
                groupParameters.Group.Id = newGroupId;
            });

            if (newGroupId > 0 && groupParameters.IsValid())
            {
                await this.SetGroupParameters(newGroupId);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Updates an existing group record in the database
        /// </summary>
        /// <param name="groupParameters"></param>
        /// <returns></returns>
        public GroupParameters EditGroup(GroupParameters groupParameters)
        {
            throw new NotImplementedException();
        }

    }
}
