﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared.DTO;
using ServiceStack.OrmLite;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class UserHelper : IUserHelper
    {

        private readonly IHttpContextAccessor _HttpContextAccessor;
        public UserHelper(IHttpContextAccessor httpContextAccessor)
        {
            _HttpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Method to get current userId from the http session.
        /// </summary>
        /// <returns></returns>
        public Guid getCurrentUserId()
        {
            return new Guid(_HttpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
        }

        /// <summary>
        /// Gets a list of the current users connected organization
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetUserOrganizations()
        {
            var output = new List<Organization>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var query1 = Db.From<Organization>()
                    .Join<User_Organization>((o, uo) => o.Id == uo.Organization_Id && uo.AspNetUsers_Id == getCurrentUserId())
                    .SelectDistinct();

                var organizationsAsAdmin = Db.Select(query1).ToDictionary(x => x.Id, y => y);

                var query2 = Db.From<Organization>()
                   .Join<Group>((o, g) => o.Id == g.Organization_Id)
                   .Join<Group, User_Group>((g, ug) => g.Id == ug.Group_Id && ug.AspNetUsers_Id == getCurrentUserId())
                   .SelectDistinct();

                var organizationsAsGroupAdmin = Db.Select(query2).ToDictionary(x => x.Id, y => y);
                output = organizationsAsGroupAdmin.Select(x => x.Value).ToList();
                foreach (var group in organizationsAsAdmin)
                {
                    if (!organizationsAsGroupAdmin.ContainsKey(group.Key))
                    {
                        output.Add(group.Value);
                    }
                }
            });

            return output;
        }

        /// <summary>
        /// Gets a list of the current users connected entities
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetUserEntities()
        {
            var output = new List<Entity>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                output = Db.Select(Db.From<Entity>()
                    .Join<User_Entity>((e, ue) => e.Id == ue.Entity_Id && ue.AspNetUsers_Id == getCurrentUserId())
                    );
            });

            return output;
        }
    }
}
