﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Linq;
using BlazorSecureLogin.Shared;
using ServiceStack.OrmLite;
using BlazorSecureLogin.Server.Services.Contracts;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class EntityHelper : BaseHelper, IEntityHelper
    {
        public EntityHelper(IUserHelper userHelper) : base(userHelper) { }

        /// <summary>
        /// Gets entity parameters by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EntityParameters GetEntityParameters(long id)
        {
            var output = new EntityParameters();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                try
                {
                    output.Entity = Db.Single(
                        Db.From<Entity>()
                        .Where(x => x.Id == id));

                    if (_HasFullAccess(id, output.Entity.Organization_Id))
                    {
                        output = null;
                    }
                }
                catch (Exception e)
                {
                    throw (e);
                }
            });

            return output;
        }

        /// <summary>
        /// Creates a new entity record in the database
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        public EntityParameters CreateEntity(EntityParameters entityParameters)
        {
            long newEntityId = 0;

            if (!_GroupOwnerByOrganization_Id(entityParameters.Entity.Organization_Id) || !_OrganizationOwner(entityParameters.Entity.Organization_Id))
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                entityParameters.Entity.CreatedBy = UserHelper.getCurrentUserId();
                entityParameters.Entity.CreateDate = DateTime.UtcNow;
                newEntityId = Db.Insert(entityParameters.Entity, true);
                entityParameters.Entity.Id = newEntityId;
            });

            if (newEntityId > 0 && entityParameters.IsValid())
            {
                return entityParameters;
            }

            return null;
        }

        /// <summary>
        /// Updates a specific entity record in the database
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        public EntityParameters UpdateEntity(EntityParameters entityParameters)
        {
            if (!_HasFullAccess(entityParameters.Entity.Id, entityParameters.Entity.Organization_Id))
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                Db.Update(entityParameters.Entity);
            });

            // Read replica warning
            return GetEntityParameters(entityParameters.Entity.Id);
        }

        /// <summary>
        /// Deletes a entity record by entityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public bool DeleteEntity(long entityId)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entity = Db.SingleById<Entity>(entityId);
                if (entity == null || !this._HasFullAccess(entityId, entity.Organization_Id))
                {
                    return;
                }
                // If we put DELETE ON CASCADE on the tables, we can replace below code with: Db.DeleteById<Group>(groupParameters.Group.Id);
                var query1 = Db.From<User_Entity>().Where(x => x.Entity_Id == entityId);
                Db.Delete(query1);
                var query2 = Db.From<Entity_Group>().Where(x => x.Entity_Id == entityId);
                Db.Delete(query2);
                var query3 = Db.From<Entity>().Where(x => x.Id == entityId);
                Db.Delete(query3);

                output = true;
            });

            return output;
        }

        /// <summary>
        /// Gets organizationparameters  for the provided entityId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OrganizationParameters GetEntityOrganization (long id)
        {
            var output = new OrganizationParameters();

            if (!IsEntityOwner(id))
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                try
                {
                    output.Organization = Db.Single(
                        Db.From<Organization>()
                        .Join<Entity>((o, e) => o.Id == e.Organization_Id)
                        .Where<Entity>(x => x.Id == id));
                }
                catch (Exception e)
                {
                    throw (e);
                }
            });

            return output;
        }

        #region Private access methods
        private bool _HasFullAccess (long entityId, long organizationId)
        {
            return !IsEntityOwner(entityId) || _GroupOwnerByOrganization_Id(organizationId) || _OrganizationOwner(organizationId);
        }
        private bool IsEntityOwner(long entityId)
        {
            var user_id = UserHelper.getCurrentUserId();
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var query2 = Db.From<User_Entity>()
                    .Where(x => x.AspNetUsers_Id == UserHelper.getCurrentUserId() && x.Entity_Id == entityId);
                output = Db.Exists(query2);
            });
            return output;
        }
        #endregion
    }
}
