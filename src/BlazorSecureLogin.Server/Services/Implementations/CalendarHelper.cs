﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class CalendarHelper : BaseHelper, ICalendarHelper
    {
        public CalendarHelper(IUserHelper userhelper) : base(userhelper) { }

        /// <summary>
        /// Creates a new calendar appointment.
        /// </summary>
        /// <param name="calendarPutParameters"></param>
        /// <returns></returns>
        public bool? CreateCalendarData(CalendarPutParameters calendarPutParameters)
        {
            bool? output = false;
            if (!calendarPutParameters.CalendarType.HasValue)
            {
                return null;
            }

            switch (calendarPutParameters.CalendarType.Value)
            {
                case CalendarType.Organization:
                    if (!_OrganizationOwner(calendarPutParameters.CalendarData.SourceOrganization_Id))
                    {
                        return null;
                    }
                    break;
                case CalendarType.Entity:
                case CalendarType.Group:
                    if (!_OrganizationOwner(calendarPutParameters.CalendarData.SourceOrganization_Id) && !(calendarPutParameters.CalendarData.SourceGroup_Id.HasValue && _GroupOwnerByOrganization_Id(calendarPutParameters.CalendarData.SourceGroup_Id.Value)))
                    {
                        return null;
                    }
                    break;
                default:
                    return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                // assign posted params to DTO
                calendarPutParameters.CalendarData.CreateDate = DateTime.Now;
                calendarPutParameters.CalendarData.StartTime = calendarPutParameters.CalendarData.StartTime.ToLocalTime();
                calendarPutParameters.CalendarData.EndTime = calendarPutParameters.CalendarData.EndTime.ToLocalTime();
                calendarPutParameters.CalendarData.CreatedBy = UserHelper.getCurrentUserId();


                // check datetime to be at least 15 min apart.
                var dateTimeCheck = calendarPutParameters.CalendarData.StartTime.AddMinutes(15);
                if (calendarPutParameters.CalendarData.EndTime.Ticks < dateTimeCheck.Ticks)
                {
                    calendarPutParameters.CalendarData.EndTime = dateTimeCheck;
                }

                // Insert the calendar data to DB, get the ID for join tables.
                var calendarDataId = Db.Insert(calendarPutParameters.CalendarData, true);

                List<CalendarDataRelation> calendarDataRelations = new List<CalendarDataRelation>();
                List<CalendarDataAdminRelation> calendarDataAdminRelations = new List<CalendarDataAdminRelation>();
                switch (calendarPutParameters.CalendarType.Value)
                {
                    case CalendarType.Organization:
                        calendarDataAdminRelations = calendarPutParameters.Ids.Select(x => new CalendarDataAdminRelation
                        {
                            Organization_Id = x,
                            CalendarData_Id = calendarDataId,
                            AllGroups = calendarPutParameters.AllGroups
                        }).ToList();

                        if (!calendarPutParameters.AdminCalendar)
                        {
                            calendarDataRelations = calendarDataAdminRelations.Select(x => new CalendarDataRelation
                            {
                                CalendarData_Id = calendarDataId,
                                Organization_Id = x.Organization_Id
                            }).ToList();
                        }
                        break;
                    case CalendarType.Group:
                        calendarDataAdminRelations = calendarPutParameters.Ids.Select(x => new CalendarDataAdminRelation
                        {
                            Group_Id = x,
                            CalendarData_Id = calendarDataId
                        }).ToList();

                        if (!calendarPutParameters.AdminCalendar)
                        {
                            calendarDataRelations = calendarDataAdminRelations.Select(x => new CalendarDataRelation
                            {
                                CalendarData_Id = calendarDataId,
                                Group_Id = x.Group_Id
                            }).ToList();
                        }
                        break;
                    case CalendarType.Entity:
                        calendarDataAdminRelations.Add(new CalendarDataAdminRelation
                        {
                            Group_Id = calendarPutParameters.CalendarData.SourceGroup_Id,
                            CalendarData_Id = calendarDataId
                        });

                        if (!calendarPutParameters.AdminCalendar)
                        {
                            calendarDataRelations = calendarPutParameters.Ids.Select(x => new CalendarDataRelation
                            {
                                Entity_Id = x,
                                CalendarData_Id = calendarDataId
                            }).ToList();
                        }
                        break;
                    default:
                        return;
                }

                if (calendarDataAdminRelations.Any())
                {
                    Db.InsertAll(calendarDataAdminRelations);
                }
                if (calendarDataRelations.Any())
                {
                    Db.InsertAll(calendarDataRelations);
                }

                output = Db.Exists(Db.From<CalendarData>().Where(x => x.Id == calendarDataId));
            });

            return output;
        }

        /// <summary>
        /// Gets a list of calendar appointments based on calendarPutParameters
        /// </summary>
        /// <param name="calendarParameters"></param>
        /// <returns></returns>
        public async Task<List<CalendarData>> GetCalendarDatas(CalendarParameters calendarParameters)
        {
            HashSet<long> eventIds = new HashSet<long>();
            List<CalendarData> output = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var query = Db.From<CalendarData>();
                switch (calendarParameters.CalendarType)
                {
                    case CalendarType.Organization:
                        query.Join<CalendarDataAdminRelation>((cd, cdar) => cd.Id == cdar.CalendarData_Id);
                        query.Where<CalendarDataAdminRelation>(x => x.Organization_Id == calendarParameters.Id && x.AllGroups == true);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        query = null;
                        if (_OrganizationOwner(calendarParameters.Id))
                        {
                            query = Db.From<CalendarData>();
                            query.Join<CalendarDataAdminRelation>((cd, cdar) => cdar.AllGroups == false && cdar.Organization_Id == calendarParameters.Id && cdar.Group_Id == null);
                            query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                            eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                            query = null;
                        }

                        query = Db.From<CalendarData>();
                        query.Join<User_Group>((cd, ug) => ug.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Group, CalendarDataAdminRelation>((ug, cdar) => cdar.Group_Id == ug.Group_Id);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);

                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        break;
                    case CalendarType.Group:
                        if (!_GroupOwnerBy(calendarParameters.Id) && !_OrganizationOwnerByGroup_Id(calendarParameters.Id))
                        {
                            break;
                        }

                        query.Join<CalendarDataAdminRelation>((cd, cdar) => cdar.Group_Id == calendarParameters.Id);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));

                        break;
                    case CalendarType.Entity:
                        break;
                    case CalendarType.User:
                        // by entity
                        query.Join<CalendarDataRelation>((cd, cdar) => cd.Id == cdar.CalendarData_Id);
                        query.Join<CalendarDataRelation, User_Entity>((cdr, ue) => ue.AspNetUsers_Id == UserHelper.getCurrentUserId() && cdr.Entity_Id == ue.Entity_Id);
                        query.Select<CalendarDataRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        query = null;
                        // by entity group
                        query = Db.From<CalendarData>();
                        query.Join<CalendarDataRelation>((cd, cdar) => cd.Id == cdar.CalendarData_Id);
                        query.Join<User_Entity>((cdar, ue) => ue.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Entity, Entity_Group>((ue, eg) => ue.Entity_Id == eg.Entity_Id);
                        query.Where<CalendarDataRelation, Entity_Group>((cdr, eg) => cdr.Group_Id == eg.Group_Id);
                        query.Select<CalendarDataRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        query = null;
                        // by entity organization
                        query = Db.From<CalendarData>();
                        query.Join<CalendarDataRelation>((cd, cdar) => cd.Id == cdar.CalendarData_Id);
                        query.Join<User_Entity>((cdar, ue) => ue.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Entity, Entity>((ue, e) => ue.Entity_Id == e.Id);
                        query.Where<CalendarDataRelation, Entity>((cdr, e) => cdr.Organization_Id == e.Organization_Id);
                        query.Select<CalendarDataRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        query = null;
                        // by organization owner
                        query = Db.From<CalendarData>();
                        query.Join<User_Organization>((cd, ug) => ug.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Organization, CalendarDataAdminRelation>((uo, cdar) => cdar.Organization_Id == uo.Organization_Id);
                        query.Where<CalendarDataAdminRelation>(x => x.AllGroups == false);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        query = null;
                        // by group owner
                        query = Db.From<CalendarData>();
                        query.Join<User_Group>((cd, ug) => ug.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Group, CalendarDataAdminRelation>((ug, cdar) => cdar.Group_Id == ug.Group_Id);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));
                        // all groups
                        query = Db.From<CalendarData>();
                        query.Join<User_Group>((cd, ug) => ug.AspNetUsers_Id == UserHelper.getCurrentUserId());
                        query.Join<User_Group, Group>((ug, g) => ug.Group_Id == g.Id);
                        query.Join<Group, CalendarDataAdminRelation>((g, cdar) => cdar.Organization_Id == g.Organization_Id && cdar.AllGroups == true);
                        query.Select<CalendarDataAdminRelation>(x => x.CalendarData_Id);
                        eventIds.UnionWith(Db.ColumnDistinct<long>(query));

                        break;
                    default:
                        break;
                }

                    output = Db.Select(Db.From<CalendarData>().Where(x => Sql.In(x.Id, eventIds)));
            });

            return output;
        }
    }
}
