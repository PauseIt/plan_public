﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorSecureLogin.Server.Models;
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using BlazorSecureLogin.Shared.DTO.Visualizations;
using ServiceStack.OrmLite;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class OrganizationHelper : BaseHelper, IOrganizationHelper
    {
        #region Fields & constructors
        public OrganizationHelper(IUserHelper userHelper) : base(userHelper) { }
        private OrganizationParameters _OrganizationParameters { get; set; } = null;
        public OrganizationParameters OrganizationParameters
        {
            get => this._OrganizationParameters;
        }
        private List<User_Organization> _User_Organizations { get; set; } = null;
        public List<User_Organization> User_Organizations
        {
            get => (_User_Organizations != null) ? _User_Organizations : _User_Organizations = LoadOrganizationUsers();
        }

        private List<GroupParameters> _GroupsAsAdmin { get; set; } = null;
        public List<GroupParameters> GroupsAsAdmin
        {
            get => (_GroupsAsAdmin != null) ? _GroupsAsAdmin : _GroupsAsAdmin = LoadGroupsAsAdmin();
        }

        private List<GroupParameters> _AllGroups { get; set; } = null;
        public List<GroupParameters> AllGroups
        {
            get => (_AllGroups != null) ? _AllGroups : _AllGroups = LoadAllGroups();
        }
        #endregion

        #region Organization
        /// <summary>
        /// Method to check if class is properly initialized
        /// </summary>
        /// <returns></returns>
        public bool IsInitialized()
        {
            return this._OrganizationParameters != null;
        }

        /// <summary>
        /// Method to check if current OrganizationParameters are valid
        /// </summary>
        /// <returns></returns>
        public bool IsValidOrganizationParameters()
        {
            return this.OrganizationParameters.IsValid();
        }

        /// <summary>
        /// Method to check if current user is organization owner by organizationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool OrganizationOwner(long id)
        {
            return this._OrganizationOwner(id);
        }

        /// <summary>
        /// Changes the current OrganizationParameters
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task SetOrganizationParameters(long Id)
        {
            this._OrganizationParameters = null;
            if (!this._HasAccess(Id))
            {
                return;
            }
            this._OrganizationParameters = new OrganizationParameters();

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                try
                {
                    this._OrganizationParameters.Organization = Db.Single(
                        Db.From<Organization>()
                        .Where(x => x.Id == Id));
                }
                catch (Exception e)
                {
                    throw (e);
                }
            });

        }

        /// <summary>
        /// Gets the current organizationParameters
        /// </summary>
        /// <returns></returns>
        public OrganizationParameters LoadOrganizationParameters()
        {
            if (!this.IsInitialized())
            {
                return null;
            }

            return this.OrganizationParameters;
        }

        /// <summary>
        /// Creates a new Organization record in the database
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        public async Task<bool> CreateOrganization(OrganizationParameters organizationParameters)
        {
            long newOrganizationId = 0;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                organizationParameters.Organization.CreatedBy = UserHelper.getCurrentUserId();
                organizationParameters.Organization.CreateDate = DateTime.UtcNow;
                newOrganizationId = Db.Insert(organizationParameters.Organization, true);
                organizationParameters.Organization.Id = newOrganizationId;
                var UserOrganization = new User_Organization { AspNetUsers_Id = UserHelper.getCurrentUserId(), Organization_Id = newOrganizationId };
                Db.Insert(UserOrganization);
            });

            if (newOrganizationId > 0 && organizationParameters.IsValid())
            {
                // Read replica warning!
                await this.SetOrganizationParameters(newOrganizationId);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Updates an existing Organization record
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        public OrganizationParameters UpdateOrganization(OrganizationParameters organizationParameters)
        {
            if (!this._OrganizationOwner(organizationParameters.Organization.Id) || !organizationParameters.IsValid())
            {
                return null;
            }

            OrganizationParameters updatedOrganizationParameters = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                organizationParameters.Organization.Title = string.IsNullOrEmpty(organizationParameters.Organization.Title) ? null : organizationParameters.Organization.Title;
                organizationParameters.Organization.Name = string.IsNullOrEmpty(organizationParameters.Organization.Name) ? null : organizationParameters.Organization.Name;
                Db.Update(organizationParameters.Organization);
                updatedOrganizationParameters = new OrganizationParameters();
                updatedOrganizationParameters.Organization = Db.SingleById<Organization>(organizationParameters.Organization.Id);
            });

            return updatedOrganizationParameters;
        }

        /// <summary>
        /// Gets all organization owners for the currently selected organization.
        /// </summary>
        /// <returns></returns>
        private List<User_Organization> LoadOrganizationUsers()
        {
            if (!this.IsInitialized())
            {
                return null;
            }

            List<User_Organization> user_Organizations = new List<User_Organization>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                user_Organizations = Db.Select(
                    Db.From<User_Organization>().
                    Where(x => x.Organization_Id == this.OrganizationParameters.Organization.Id)
                    );
            });

            return user_Organizations;
        }

        #endregion

        #region Group
        /// <summary>
        /// Gets the list of all groups in the current organization where current user is assosiated as administrator
        /// </summary>
        /// <returns></returns>
        private List<GroupParameters> LoadGroupsAsAdmin()
        {
            if (!this.IsInitialized())
            {
                return null;
            }

            List<GroupParameters> output = new List<GroupParameters>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var groupQuery = Db.From<Group>()
                    .Join<User_Group>((g, gu) => g.Id == gu.Group_Id && gu.AspNetUsers_Id == UserHelper.getCurrentUserId())
                    .Where(x => x.Organization_Id == this.OrganizationParameters.Organization.Id);
                var groups = Db.Select(groupQuery).ToDictionary(x => x.Id, y => y);

                foreach (var group in groups)
                {
                    var groupParameter = new GroupParameters();
                    groupParameter.Group = group.Value;
                    output.Add(groupParameter);
                }
            });

            return output;
        }

        /// <summary>
        /// Gets the list of all groups in the current organization 
        /// </summary>
        /// <returns></returns>
        private List<GroupParameters> LoadAllGroups()
        {
            if (!this.IsInitialized() || !this.OrganizationOwner(this.OrganizationParameters.Organization.Id))
            {
                return null;
            }

            List<GroupParameters> output = new List<GroupParameters>();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var groupQuery = Db.From<Group>()
                    .Where(x => x.Organization_Id == this.OrganizationParameters.Organization.Id);
                var groups = Db.Select(groupQuery).ToDictionary(x => x.Id, y => y);

                foreach (var group in groups)
                {
                    var groupParameter = new GroupParameters();
                    groupParameter.Group = group.Value;
                    output.Add(groupParameter);
                }
            });

            return output;
        }

        /// <summary>
        /// Gets the list of all groups in the current organization with extended information about administrators and creator.
        /// </summary>
        /// <returns></returns>
        public List<GroupVisualization> GetAllGroupVisualizations()
        {
            var output = new List<GroupVisualization>();
            if (!_HasAccess())
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
           {
               var groups = Db.Select(
                       Db.From<Group>().Where(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                   ).ToDictionary(x => x.Id, y => y);

               var groupAdministrators = Db.Select<dynamic>(
                       Db.From<User_Group>()
                       .Join<Group>((ug, g) => ug.Group_Id == g.Id)
                       .Join<UserData>((ug, ud) => ug.AspNetUsers_Id == ud.UserId)
                       .Where<Group>(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                       .Select<UserData, User_Group>((ud, ug) => new { GroupId = ug.Group_Id, Name = (ud.FirstName ?? "") + " " + (ud.LastName ?? "") })
                   ).Select(x => new
                   {
                       GroupId = (long)x.GroupId,
                       Name = (string)x.Name
                   }).GroupBy(x => x.GroupId).ToDictionary(x => x.Key);

               var groupCreators = Db.Select<dynamic>(
                       Db.From<UserData>()
                       .Join<Group>((ud, g) => ud.UserId == g.CreatedBy)
                       .Where<Group>(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                       .Select<UserData, Group>((ud, g) => new { GroupId = g.Id, CreatorName = (ud.FirstName ?? "") + " " + (ud.LastName ?? "") })
                   ).Select(x => new
                   {
                       GroupId = (long)x.GroupId,
                       Name = (string)x.CreatorName
                   }).ToDictionary(x => x.GroupId, y => y.Name);

               foreach (var group in groups)
               {
                   var groupVisualization = new GroupVisualization();
                   groupVisualization.Group = group.Value;
                   if (groupAdministrators.ContainsKey(group.Key))
                   {
                       groupVisualization.Administrators = groupAdministrators[group.Key].Select(x => x.Name).ToList();
                   }

                   if (groupCreators.ContainsKey(group.Key))
                   {
                       groupVisualization.CreatedByName = groupCreators[group.Key];
                   }

                   output.Add(groupVisualization);
               }
           });

            return output;
        }

        /// <summary>
        /// Creates a new group record in the database
        /// </summary>
        /// <param name="groupParameters"></param>
        /// <returns></returns>
        public GroupParameters CreateGroup(GroupParameters groupParameters)
        {
            if (!this._HasAccess(groupParameters.Group.Organization_Id) || !groupParameters.IsValid())
            {
                return null;
            }

            long newGroupId = 0;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                groupParameters.Group.CreateDate = DateTime.UtcNow;
                groupParameters.Group.CreatedBy = UserHelper.getCurrentUserId();
                newGroupId = Db.Insert(groupParameters.Group, true);
                groupParameters.Group.Id = newGroupId;
            });

            if (newGroupId > 0 && groupParameters.IsValid())
            {
                return groupParameters;
            }
            return null;
        }

        /// <summary>
        /// Updates an existing group record in the database
        /// </summary>
        /// <param name="groupParameters"></param>
        /// <returns></returns>
        public GroupParameters UpdateGroup(GroupParameters groupParameters)
        {
            if (!this._HasAccess(groupParameters.Group.Organization_Id) || !groupParameters.IsValid())
            {
                return null;
            }

            GroupParameters updatedGroupParameters = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                Db.Update(groupParameters.Group);
                updatedGroupParameters = new GroupParameters();
                updatedGroupParameters.Group = Db.SingleById<Group>(groupParameters.Group.Id);
            });

            return updatedGroupParameters;
        }

        /// <summary>
        /// Deletes a group record in the database by GroupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGroup(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var group = Db.SingleById<Group>(id);
                if (group == null || !this._OrganizationOwner(group.Organization_Id))
                {
                    return;
                }
                // If we put DELETE ON CASCADE on the tables, we can replace below code with: Db.DeleteById<Group>(groupParameters.Group.Id);
                var query1 = Db.From<User_Group>().Where(x => x.Group_Id == id);
                Db.Delete(query1);
                var query2 = Db.From<Entity_Group>().Where(x => x.Group_Id == id);
                Db.Delete(query2);
                var query3 = Db.From<Group>().Where(x => x.Id == id);
                Db.Delete(query3);

                output = true;
            });

            return output;
        }

        /// <summary>
        /// Connects a user with a group
        /// </summary>
        /// <param name="userInviteParameters"></param>
        /// <returns></returns>
        public bool? GroupInviteUser(UserInviteParameters userInviteParameters)
        {
            bool? output = null;

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var group = Db.SingleById<Group>(userInviteParameters.ReferenceId);
                if (group == null || !this._OrganizationOwner(group.Organization_Id))
                {
                    output = false;
                    return;
                }

                var userId = getUserGuidByEmail(userInviteParameters.Email);
                if (userId == new Guid())
                {
                    output = false;
                    return;
                }

                var existQuery = Db.From<User_Group>().Where(x => x.AspNetUsers_Id == userId && x.Group_Id == userInviteParameters.ReferenceId);
                if (Db.Exists(existQuery))
                {
                    output = false;
                    return;
                }

                Db.Insert(new User_Group
                {
                    AspNetUsers_Id = userId,
                    Group_Id = userInviteParameters.ReferenceId
                });

                output = true;
            });

            return output;
        }

        /// <summary>
        ///  Returns true if current user is group administrator by OrganizationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GroupOwner (long id)
        {
            return this._GroupOwnerByOrganization_Id(id);
        }

        #endregion

        #region Entity
        /// <summary>
        /// Creates a new entity record
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        public EntityParameters CreateEntity(EntityParameters entityParameters)
        {
            if (!this._HasAccess(entityParameters.Entity.Organization_Id) || !entityParameters.IsValid())
            {
                return null;
            }

            long newEntityId = 0;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                entityParameters.Entity.CreateDate = DateTime.UtcNow;
                entityParameters.Entity.CreatedBy = UserHelper.getCurrentUserId();
                newEntityId = Db.Insert(entityParameters.Entity, true);
                entityParameters.Entity.Id = newEntityId;
            });

            if (newEntityId > 0 && entityParameters.IsValid())
            {
                return entityParameters;
            }

            return null;
        }

        /// <summary>
        /// Updates an existing Entity record in the database
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        public EntityParameters UpdateEntity(EntityParameters entityParameters)
        {
            if (!this._HasAccess(entityParameters.Entity.Organization_Id) || !entityParameters.IsValid())
            {
                return null;
            }

            EntityParameters updatedEntityParameters = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                Db.Update(entityParameters.Entity);
                updatedEntityParameters = new EntityParameters();
                updatedEntityParameters.Entity = Db.SingleById<Entity>(entityParameters.Entity.Id);
            });

            return updatedEntityParameters;
        }

        /// <summary>
        /// Deletes an entity record in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteEntity(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entity = Db.SingleById<Entity>(id);
                if (entity == null || !this._HasAccess(entity.Organization_Id))
                {
                    return;
                }
                // If we put DELETE ON CASCADE on the tables, we can replace below code with: Db.DeleteById<Entity>(entityParameters.Entity.Id);
                var query1 = Db.From<User_Entity>().Where(x => x.Entity_Id == id);
                Db.Delete(query1);
                var query2 = Db.From<Entity_Group>().Where(x => x.Entity_Id == id);
                Db.Delete(query2);
                var query3 = Db.From<Entity>().Where(x => x.Id == id);
                Db.Delete(query3);

                output = true;
            });

            return output;
        }

        /// <summary>
        /// Connects a user and an entity
        /// </summary>
        /// <param name="userInviteParameters"></param>
        /// <returns></returns>
        public bool? EntityInviteUser(UserInviteParameters userInviteParameters)
        {
            bool? output = null;

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entity = Db.SingleById<Entity>(userInviteParameters.ReferenceId);
                if (entity == null || !this._HasAccess(entity.Organization_Id))
                {
                    output = false;
                    return;
                }

                var userId = getUserGuidByEmail(userInviteParameters.Email);
                if (userId == new Guid())
                {
                    output = false;
                    return;
                }

                var existQuery = Db.From<User_Entity>().Where(x => x.AspNetUsers_Id == userId && x.Entity_Id == userInviteParameters.ReferenceId);
                if (Db.Exists(existQuery))
                {
                    output = false;
                    return;
                }

                Db.Insert(new User_Entity
                {
                    AspNetUsers_Id = userId,
                    Entity_Id = userInviteParameters.ReferenceId
                });

                output = true;
            });

            return output;
        }

        /// <summary>
        /// Gets a list of all entities in the current organization
        /// </summary>
        /// <returns></returns>
        public List<EntityParameters> GetOrganizationEntities()
        {
            var output = new List<EntityParameters>();
            if (!_HasAccess())
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entities = Db.Select(
                        Db.From<Entity>().Where(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                    ).ToDictionary(x => x.Id, y => y);

                foreach (var entity in entities)
                {
                    var entityParameters = new EntityParameters();
                    entityParameters.Entity = entity.Value;
                    output.Add(entityParameters);
                }
            });

            return output;
        }

        /// <summary>
        /// Connects entities and groups
        /// </summary>
        /// <param name="entity_Group"></param>
        /// <returns></returns>
        public bool? LinkEntityAndGroup (Entity_Group entity_Group)
        {
            bool? output = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entity = Db.SingleById<Entity>(entity_Group.Entity_Id);
                var group = Db.SingleById<Group>(entity_Group.Group_Id);
                if (entity == null || group == null || !this._HasAccess(entity.Organization_Id))
                {
                    output = false;
                    return;
                }

                if (entity.Organization_Id != group.Organization_Id)
                {
                    output = false;
                    return;
                }

                var existQuery = Db.From<Entity_Group>().Where(x => x.Group_Id == entity_Group.Group_Id && x.Entity_Id == entity_Group.Entity_Id);
                if (Db.Exists(existQuery))
                {
                    output = false;
                    return;
                }

                Db.Insert(entity_Group);
                output = true;
            });

            return output;
        }

        /// <summary>
        /// Gets all Ids of entities within a provided groupId
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public List<long> GetEntityIdsFromGroupId(long groupId)
        {
            var output = new List<long>();

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var query = Db.From<Entity>()
                    .Join<Entity_Group>((e, eg) => e.Id == eg.Entity_Id)
                    .Where<Entity_Group>(x => x.Group_Id == groupId)
                    .Select(x => x.Id);

                output = Db.Column<long>(query);
            });

            return output;
        }

        /// <summary>
        /// Gets a list of all entities in the current organization with extended information
        /// </summary>
        /// <returns></returns>
        public List<EntityVisualization> GetAllEntityVisualizations()
        {
            var output = new List<EntityVisualization>();
            if (!_HasAccess())
            {
                return null;
            }

            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var entities = Db.Select(
                        Db.From<Entity>().Where(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                    ).ToDictionary(x => x.Id, y => y);

                var entityGroups = Db.Select<dynamic>(
                        Db.From<Entity_Group>()
                        .Join<Entity>((ue, e) => ue.Entity_Id == e.Id)
                        .Join<Group>((eg, g) => eg.Group_Id == g.Id)
                        .Where<Entity>(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                        .Select<Entity, Group>((e, g) => new { EntityId = e.Id, Name = g.Title })
                    ).Select(x => new
                    {
                        EntityId = (long)x.EntityId,
                        Name = (string)x.Name
                    }).GroupBy(x => x.EntityId).ToDictionary(x => x.Key);

                var entityOwners = Db.Select<dynamic>(
                        Db.From<User_Entity>()
                        .Join<Entity>((ue, e) => ue.Entity_Id == e.Id)
                        .Join<UserData>((ue, ud) => ue.AspNetUsers_Id == ud.UserId)
                        .Where<Entity>(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                        .Select<UserData, User_Entity>((ud, ue) => new { EntityId = ue.Entity_Id, Name = (ud.FirstName ?? "") + " " + (ud.LastName ?? "") })
                    ).Select(x => new
                    {
                        EntityId = (long)x.EntityId,
                        Name = (string)x.Name
                    }).GroupBy(x => x.EntityId).ToDictionary(x => x.Key);

                var EntityCreators = Db.Select<dynamic>(
                        Db.From<UserData>()
                        .Join<Entity>((ud, e) => ud.UserId == e.CreatedBy)
                        .Where<Entity>(x => x.Organization_Id == this._OrganizationParameters.Organization.Id)
                        .Select<UserData, Entity>((ud, e) => new { EntityId = e.Id, CreatorName = (ud.FirstName ?? "") + " " + (ud.LastName ?? "") })
                    ).Select(x => new
                    {
                        EntityId = (long)x.EntityId,
                        Name = (string)x.CreatorName
                    }).ToDictionary(x => x.EntityId, y => y.Name);

                foreach (var entity in entities)
                {
                    var entityVisualization = new EntityVisualization();
                    entityVisualization.Entity = entity.Value;

                    if (entityGroups.ContainsKey(entity.Key))
                    {
                        entityVisualization.LinkedGroups = entityGroups[entity.Key].Select(x => x.Name).ToList();
                    }

                    if (entityOwners.ContainsKey(entity.Key))
                    {
                        entityVisualization.Owners = entityOwners[entity.Key].Select(x => x.Name).ToList();
                    }

                    if (EntityCreators.ContainsKey(entity.Key))
                    {
                        entityVisualization.CreatedByName = EntityCreators[entity.Key];
                    }

                    output.Add(entityVisualization);
                }
            });

            return output;
        }

        #endregion

        #region Private access methods
        private bool _HasAccess()
        {
            return this._HasAccess(this.OrganizationParameters.Organization.Id);
        }

        private bool _HasAccess(long id)
        {
            return _OrganizationOwner(id) || _GroupOwnerByOrganization_Id(id);
        }

        #endregion

        #region Private helper methods
        private Guid getUserGuidByEmail(string email)
        {
            Guid output = new Guid();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                // This select is done with hardcoded SQL to avoid exceeding the free-quota of 10 Ormlite tables
                output = Db.SqlScalar<Guid>("SELECT [Id] FROM [dbo].[AspNetUsers] [anu] WHERE [anu].Email = @Email", new { Email = email });
            });

            return output;
        }
        #endregion
    }
}
