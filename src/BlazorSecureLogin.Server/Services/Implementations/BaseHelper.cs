﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared.DTO;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Services.Implementations
{
    public class BaseHelper
    {   
        public IUserHelper UserHelper { get; set; }
        public BaseHelper(IUserHelper userHelper)
        {
            this.UserHelper = userHelper;
        }

        /// <summary>
        /// returns true if current user is organiszation owner in provided organization.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _OrganizationOwner(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Organization>()
                    .Where(x => x.AspNetUsers_Id == UserHelper.getCurrentUserId() && x.Organization_Id == id);

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }

        /// <summary>
        /// returns true if current user is organization owner in the organization by groupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _OrganizationOwnerByGroup_Id(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Organization>()
                    .Join<Group>((uo, g) => uo.Organization_Id == g.Organization_Id)
                    .Where<User_Organization, Group>((uo, g) => uo.AspNetUsers_Id == UserHelper.getCurrentUserId() && g.Id == id);

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }

        /// <summary>
        /// returns true if current user is organiszation owner in the organization by entityId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _OrganizationOwnerByEntity_Id(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Organization>()
                    .Join<Entity>((uo, e) => uo.Organization_Id == e.Organization_Id)
                    .Where<User_Organization, Entity>((uo, e) => uo.AspNetUsers_Id == UserHelper.getCurrentUserId() && e.Organization_Id == id);

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }

        /// <summary>
        /// returns true if current user is organiszation owner in provided organization.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _GroupOwnerBy(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Group>()
                    .Where(x => x.AspNetUsers_Id == UserHelper.getCurrentUserId() && x.Group_Id == id);

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }

        /// <summary>
        /// Returns true if current user is group administrator in any groups in a provided organizationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _GroupOwnerByOrganization_Id(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Group>()
                    .Join<Group>((ug, g) => ug.Group_Id == g.Id && g.Organization_Id == id)
                    .Where(x => x.AspNetUsers_Id == UserHelper.getCurrentUserId());

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }

        /// <summary>
        /// Returns true if current user is group owner by entity Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool _GroupOwnerByEntity_Id(long id)
        {
            var output = false;
            OrmLiteHelper.OpenDbConnection(Db =>
            {

                var query = Db.From<User_Group>()
                    .Join<Entity_Group>((ug, eg) => ug.Group_Id == eg.Group_Id)
                    .Where<User_Group, Entity_Group>((ug, eg) => ug.AspNetUsers_Id == UserHelper.getCurrentUserId() && eg.Entity_Id == id);

                if (Db.Exists(query))
                {
                    output = true;
                }
            });

            return output;
        }
    }
}
