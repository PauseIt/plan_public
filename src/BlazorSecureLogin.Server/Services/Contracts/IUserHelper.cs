﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Collections.Generic;

namespace BlazorSecureLogin.Server.Services.Contracts
{
    // Interface for the User helper/service
    public interface IUserHelper
    {
        Guid getCurrentUserId();
        List<Organization> GetUserOrganizations();
        List<Entity> GetUserEntities();
    }
}