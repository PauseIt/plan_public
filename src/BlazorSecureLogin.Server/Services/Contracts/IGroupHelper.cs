﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A

using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Services.Contracts
{
    // Interface for the group helper/service
    public interface IGroupHelper
    {
        GroupParameters GroupParameters { get; }
        Task SetGroupParameters(long id);
        Task<bool> CreateGroup(GroupParameters groupParameters);
        List<Entity> GroupEntities { get; }
        GroupParameters EditGroup(GroupParameters groupParameters);
        GroupParameters LoadGroupParameters();
    }
}