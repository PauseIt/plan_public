﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;

namespace BlazorSecureLogin.Server.Services.Contracts
{
    // Contract for the Calendar helper/service
    public interface ICalendarHelper
    {
        bool? CreateCalendarData(CalendarPutParameters calendarPutParameters);
        Task<List<CalendarData>> GetCalendarDatas(CalendarParameters calendarParameters);
    }
}