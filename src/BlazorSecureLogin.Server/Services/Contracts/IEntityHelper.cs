﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A

using BlazorSecureLogin.Shared;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Services.Contracts
{
    // Contract for the Entity helper/service
    public interface IEntityHelper
    {
        EntityParameters CreateEntity(EntityParameters entityParameters);
        EntityParameters GetEntityParameters(long id);
        EntityParameters UpdateEntity(EntityParameters entityParameters);
        OrganizationParameters GetEntityOrganization(long id);
        bool DeleteEntity(long id);
    }
}