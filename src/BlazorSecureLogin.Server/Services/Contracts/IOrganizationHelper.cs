﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using BlazorSecureLogin.Shared.DTO.Visualizations;

namespace BlazorSecureLogin.Server.Services.Contracts
{
    // Interface for the organisation helper/service
    public interface IOrganizationHelper
    {
        // Organization
        OrganizationParameters OrganizationParameters { get; }
        List<User_Organization> User_Organizations { get; }
        bool IsInitialized();
        bool IsValidOrganizationParameters();
        bool OrganizationOwner(long id);
        Task SetOrganizationParameters(long Id);
        Task<bool> CreateOrganization(OrganizationParameters organizationParameters);
        OrganizationParameters UpdateOrganization(OrganizationParameters organizationParameters);
        OrganizationParameters LoadOrganizationParameters();

        // Group data
        List<GroupParameters> GroupsAsAdmin { get; }
        List<GroupParameters> AllGroups { get; }
        List<GroupVisualization> GetAllGroupVisualizations();
        bool GroupOwner(long id);
        GroupParameters CreateGroup(GroupParameters groupParameters);
        GroupParameters UpdateGroup(GroupParameters groupParameters);
        bool DeleteGroup(long id);
        bool? GroupInviteUser(UserInviteParameters userInviteParameters);

        // Entity data
        EntityParameters CreateEntity(EntityParameters entityParameters);
        EntityParameters UpdateEntity(EntityParameters entityParameters);
        bool DeleteEntity(long id);
        bool? EntityInviteUser(UserInviteParameters userInviteParameters);
        List<EntityParameters> GetOrganizationEntities();
        List<long> GetEntityIdsFromGroupId(long id);
        bool? LinkEntityAndGroup(Entity_Group entity_Group);
        List<EntityVisualization> GetAllEntityVisualizations();
    }
}