﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Controllers
{
    [ApiController, Route("api/[controller]/[action]")]
    public class CalendarController : MyControllerBase
    {
        private ICalendarHelper _CalendarHelper { get; set; }
        public CalendarController(ICalendarHelper calendarHelper)
        {
            this._CalendarHelper = calendarHelper;
        }

        /// <summary>
        /// API endpoint go get calendar data
        /// </summary>
        /// <param name="calendarParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> GetCalendarDatas (CalendarParameters calendarParameters)
        {
            return ResponseHandler(await this._CalendarHelper.GetCalendarDatas(calendarParameters));
        }

        /// <summary>
        /// API endpoint to POST a new calendar data
        /// </summary>
        /// <param name="calendarData"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> CreateCalendarData(CalendarPutParameters calendarData)
        {
            return ResponseHandler(this._CalendarHelper.CreateCalendarData(calendarData));
        }
    }
}
