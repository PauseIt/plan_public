﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using BlazorSecureLogin.Shared;
using ServiceStack.OrmLite;
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace BlazorSecureLogin.Server.Controllers.User
{
    /// <summary>
    /// Custom controller that provides API functionality for the authorization process.
    /// </summary>
    [ApiController, Route("api/[controller]/[action]")]
    public class UserController : MyControllerBase
    {
        public UserController()
        {
           
        }


    }
}
