﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using Microsoft.AspNetCore.Authorization;
using BlazorSecureLogin.Server.Services.Contracts;

namespace BlazorSecureLogin.Server.Controllers.Entity
{
    [ApiController, Route("api/[controller]/[action]")]
    public class EntityController : MyControllerBase
    {
        private IEntityHelper _EntityHelper { get; set; }
        public EntityController(IEntityHelper entityHelper)
        {
            this._EntityHelper = entityHelper;
        }

        /// <summary>
        /// API endpoint to get an entity by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetEntity(long id)
        {
            return ResponseHandler(this._EntityHelper.GetEntityParameters(id));
        }

        /// <summary>
        /// API endpoint to get organization information for a given entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetEntityOrganization(long id)
        {
            return ResponseHandler(this._EntityHelper.GetEntityOrganization(id));
        }

        /// <summary>
        /// API endpoint to add a new entity
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> AddEntity(EntityParameters entityParameters)
        {
            return ResponseHandler(this._EntityHelper.CreateEntity(entityParameters));
        }

        /// <summary>
        /// API Endpoint to update an existing entity
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> UpdateEntity(EntityParameters entityParameters)
        {
            return ResponseHandler(this._EntityHelper.UpdateEntity(entityParameters));
        }

        /// <summary>
        /// API to delete an existing entity by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Authorize]
        public async Task<IActionResult> DeleteEntity(long id)
        {
            return ResponseHandler(this._EntityHelper.DeleteEntity(id));
        }
    }
}
