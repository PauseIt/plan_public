﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ServiceStack.OrmLite;
using BlazorSecureLogin.Server.Services.Contracts;

namespace BlazorSecureLogin.Server.Controllers.Organization
{
    /// <summary>
    /// API controller that supports available functions to the client when the client is working with an organization.
    /// </summary>
    [ApiController, Route("api/[controller]/[action]")]
    public class OrganizationController : MyControllerBase
    {
        private IOrganizationHelper _OrganizationHelper { get; set; }
        public OrganizationController( IOrganizationHelper organizationHelper)
        {
            this._OrganizationHelper = organizationHelper;
        }

        /// <summary>
        /// API endpoint to Create a new organization
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> AddOrganization(OrganizationParameters organizationParameters)
        {
            if ( await _OrganizationHelper.CreateOrganization(organizationParameters) )
            {
                return ResponseHandler(this._OrganizationHelper.OrganizationParameters);
            }

            return StatusCode(500);
        }

        /// <summary>
        /// API endpoint to update an existing organization
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> UpdateOrganization(OrganizationParameters organizationParameters)
        {
            return ResponseHandler(_OrganizationHelper.UpdateOrganization(organizationParameters));
        }

        /// <summary>
        /// API endpoint to validate if current user is an organization owner
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> OrganizationOwner(long id)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != id))
            {
                await _OrganizationHelper.SetOrganizationParameters(id);
            }

            return Ok(this._OrganizationHelper.OrganizationOwner(id));
        }

        /// <summary>
        /// API endpoint to validate if current user is a group owner
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GroupOwner(long id)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != id))
            {
                await _OrganizationHelper.SetOrganizationParameters(id);
            }

            return Ok(this._OrganizationHelper.GroupOwner(id));
        }

        /// <summary>
        /// API endpoint to get all current users groups as admin in a provided organizationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetGroupsAsAdmin(long id)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != id))
            {
                await _OrganizationHelper.SetOrganizationParameters(id);
            }
            
            if (this._OrganizationHelper.GroupsAsAdmin != null)
            {
                return Ok(this._OrganizationHelper.GroupsAsAdmin);
            }
            
            return StatusCode(403);
        }

        /// <summary>
        /// API endpoint to get all groups in a provided organizationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetAllGroups(long id)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != id))
            {
                await _OrganizationHelper.SetOrganizationParameters(id);
            }

            if (this._OrganizationHelper.AllGroups != null)
            {
                return Ok(this._OrganizationHelper.AllGroups);
            }

            return StatusCode(403);
        }

        /// <summary>
        /// API endpoint to set the active organization
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> SetActiveOrganization([FromBody] long id)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != id))
            {
                await _OrganizationHelper.SetOrganizationParameters(id);
            }

            return ResponseHandler(this._OrganizationHelper.OrganizationParameters);
        }

        /// <summary>
        /// API endpoint to get a list of all groups in an organization with extended informations
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> GetAllGroupVisualizations (OrganizationParameters organizationParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != organizationParameters.Organization.Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(organizationParameters.Organization.Id);
            }

            return ResponseHandler(this._OrganizationHelper.GetAllGroupVisualizations());
        }

        /// <summary>
        /// API endpoint to get a list of all entities in an organization
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> GetOrganizationEntities(OrganizationParameters organizationParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != organizationParameters.Organization.Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(organizationParameters.Organization.Id);
            }

            return ResponseHandler(this._OrganizationHelper.GetOrganizationEntities());
        }

        /// <summary>
        /// API endpoint to create a group in an organization
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> CreateGroup (GroupParameters groupParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != groupParameters.Group.Organization_Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(groupParameters.Group.Organization_Id);
            }

            return ResponseHandler(this._OrganizationHelper.CreateGroup(groupParameters));
        }

        /// <summary>
        /// API endpoint to update a group in an organization 
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> UpdateGroup (GroupParameters groupParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != groupParameters.Group.Organization_Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(groupParameters.Group.Organization_Id);
            }

            return ResponseHandler(this._OrganizationHelper.UpdateGroup(groupParameters));
        }

        /// <summary>
        /// API endpoint to delete a group by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Authorize]
        public async Task<IActionResult> DeleteGroup (long id)
        {
            return ResponseHandler(this._OrganizationHelper.DeleteGroup(id));
        }

        /// <summary>
        /// API endpoint to connect users and groups
        /// </summary>
        /// <param name="userInviteParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> GroupInviteUser(UserInviteParameters userInviteParameters)
        {
            return ResponseHandler(this._OrganizationHelper.GroupInviteUser(userInviteParameters));
        }

        /// <summary>
        /// API endpoint to create a new entity
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> CreateEntity(EntityParameters entityParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != entityParameters.Entity.Organization_Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(entityParameters.Entity.Organization_Id);
            }

            return ResponseHandler(this._OrganizationHelper.CreateEntity(entityParameters));
        }

        /// <summary>
        /// API endpoint to update an existing entity
        /// </summary>
        /// <param name="entityParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> UpdateEntity(EntityParameters entityParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != entityParameters.Entity.Organization_Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(entityParameters.Entity.Organization_Id);
            }

            return ResponseHandler(this._OrganizationHelper.UpdateEntity(entityParameters));
        }

        /// <summary>
        /// API endpoint to delete an entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Authorize]
        public async Task<IActionResult> DeleteEntity(long id)
        {
            return ResponseHandler(this._OrganizationHelper.DeleteEntity(id));
        }

        /// <summary>
        /// API endpoint to link entity to user
        /// </summary>
        /// <param name="userInviteParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> EntityInviteUser(UserInviteParameters userInviteParameters)
        {
            return ResponseHandler(this._OrganizationHelper.EntityInviteUser(userInviteParameters));
        }

        /// <summary>
        /// API endpoint to get a list of entityIds in a group by GroupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetEntityIdsFromGroupId(long id)
        {
            return ResponseHandler(this._OrganizationHelper.GetEntityIdsFromGroupId(id));
        }

        /// <summary>
        /// API endpoint to link entities and gorups
        /// </summary>
        /// <param name="entity_Group"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> LinkEntityAndGroup(Entity_Group entity_Group)
        {
            return ResponseHandler(this._OrganizationHelper.LinkEntityAndGroup(entity_Group));
        }

        /// <summary>
        /// API endpoint to get a list of all entities in a given organization with extended information
        /// </summary>
        /// <param name="organizationParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> GetAllEntityVisualizations(OrganizationParameters organizationParameters)
        {
            if (_OrganizationHelper.OrganizationParameters == null || (_OrganizationHelper.OrganizationParameters.Organization.Id != organizationParameters.Organization.Id))
            {
                await _OrganizationHelper.SetOrganizationParameters(organizationParameters.Organization.Id);
            }

            return ResponseHandler(this._OrganizationHelper.GetAllEntityVisualizations());
        }
    }

}
