﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Server.Services.Implementations;
using BlazorSecureLogin.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Controllers.Group
{
    /// <summary>
    /// Custom controller that provides API functionality for the user.
    /// </summary>
    [ApiController, Route("api/[controller]/[action]")]
    public class GroupController : MyControllerBase
    {
        private IGroupHelper _GroupHelper { get; set; }
        public GroupController(IGroupHelper groupHelper)
        {
            this._GroupHelper = groupHelper;
        }

        /// <summary>
        /// API endpoint to add a new group
        /// </summary>
        /// <param name="groupParameters"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> AddGroup(GroupParameters groupParameters)
        {
            if (await _GroupHelper.CreateGroup(groupParameters))
            {
                return ResponseHandler(this._GroupHelper.GroupParameters);
            }

            return StatusCode(404);
        }

        /// <summary>
        /// API endpoint to set the actively working group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> SetActiveGroup([FromBody] long id)
        {
            if (_GroupHelper.GroupParameters == null || (_GroupHelper.GroupParameters.Group.Id != id))
            {
                await _GroupHelper.SetGroupParameters(id);
            }

            return ResponseHandler(this._GroupHelper.GroupParameters);
        }

        /// <summary>
        /// Get group by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetGroup(long id)
        {
            if (_GroupHelper.GroupParameters == null || (_GroupHelper.GroupParameters.Group.Id != id))
            {
                await _GroupHelper.SetGroupParameters(id);
            }

            if (this._GroupHelper.GroupParameters == null)
            {
                return StatusCode(403);
            }

            return ResponseHandler(this._GroupHelper.GroupParameters.Group);
        }

        /// <summary>
        /// Get entities in a group by groupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetGroupEntities(long id)
        {
            if (_GroupHelper.GroupParameters == null || (_GroupHelper.GroupParameters.Group.Id != id))
            {
                await _GroupHelper.SetGroupParameters(id);
            }

            if (this._GroupHelper.GroupParameters == null)
            {
                return StatusCode(403);
            }

            if (this._GroupHelper.GroupEntities != null)
            {
                return Ok(this._GroupHelper.GroupEntities);
            }

            return StatusCode(403);
        }

    }
}