﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Controllers
{
    /// <summary>
    /// A custom base class for an MVC controller without view support.
    /// </summary>
    public class MyControllerBase : ControllerBase
    {
        /// <summary>
        /// ResponseHandlers to handle response from services and used to returned to client program.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected IActionResult ResponseHandler<T>(T obj) where T : new()
        {
                return obj != null
                    ? Ok(obj)
                    : StatusCode(403, new T());
        }

        protected IActionResult ResponseHandler(bool? obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }

        protected IActionResult ResponseHandler(int? obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }

        protected IActionResult ResponseHandler(long? obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }

        protected IActionResult ResponseHandler(decimal? obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }

        protected IActionResult ResponseHandler(float? obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }

        protected IActionResult ResponseHandler(string obj)
        {
            return obj != null
                ? Ok(obj)
                : StatusCode(403, obj);
        }
    }
}
