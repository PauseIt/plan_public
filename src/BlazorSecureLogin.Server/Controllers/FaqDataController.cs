﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
namespace BlazorSecureLogin.Server.Controllers
{
    /// <summary>
    /// Controller for the api endpoint for the FAQ page model.
    /// </summary>
    [Authorize, Route("api/[controller]/[action]")]
    public class FaqDataController : MyControllerBase
    {
        List<FAQS> FAQs = null;

        /// <summary>
        /// Constructor that reads the JSON feed input and converts it into a list of FAQ<answers>
        /// </summary>
        public FaqDataController()
        {
            using(StreamReader r = new StreamReader("./Controllers/FAQ_data.json"))
            {
                var jsonString = r.ReadToEnd();
                this.FAQs = JsonConvert.DeserializeObject<List<FAQS>>(jsonString);
            }
        }

        /// <summary>
        /// The API endpoint for the client to recieve the data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<FAQS> GetFaqData()
        {
            return this.FAQs;
        }

    }
}


