﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Server.Models;
using BlazorSecureLogin.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using BlazorSecureLogin.Server.Data;
using Microsoft.EntityFrameworkCore;

namespace BlazorSecureLogin.Server.Controllers
{
    /// <summary>
    /// Custom controller that provides API functionality for the authorization process.
    /// </summary>
    [ApiController, Route("api/[controller]/[action]")]
    public class AuthorizeController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AuthorizeController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        
        /// <summary>
        /// This method logs the user in.
        /// This method uses the Identity built in classes UserManager & SignInManager, that controls things as password hashing & generation of cookie to the browser.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>A HttpResult</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginParameters parameters)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                        .Select(error => error.ErrorMessage)
                                                                        .FirstOrDefault());

            var user = await _userManager.FindByEmailAsync(parameters.Email);
            if (user == null)
            {
                return BadRequest("User does not exist");
            }

            var singInResult = await _signInManager.CheckPasswordSignInAsync(user, parameters.Password, false);

            if (!singInResult.Succeeded)
            {
                return BadRequest("Invalid password");
            }

            await _signInManager.SignInAsync(user, parameters.RememberMe);
            try
            {
                return Ok();
            }
            catch (Exception e)
            {

                throw (e);
            }
        }

        /// <summary>
        /// This method is for creating a new account.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>A HttpResult</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterParameters parameters)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.Values.SelectMany(state => state.Errors)
                                                                        .Select(error => error.ErrorMessage)
                                                                        .FirstOrDefault());


            ApplicationUser user = new ApplicationUser();
            user.UserData = new UserData();
            user.UserName = await CreateUniqueUsername(parameters.UserName);
            user.Email = parameters.Email;
            var result = await _userManager.CreateAsync(user, parameters.Password);
            
            if (!result.Succeeded) return BadRequest(result.Errors.FirstOrDefault()?.Description);

            return await Login(new LoginParameters
            {
                Email = user.Email,
                Password = parameters.Password
            });
        }

        /// <summary>
        /// NOT FULLY IMPLEMENTED.
        /// This method is for resetting password and retreive a new through email.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>A HttpResult</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SendPasswordResetEmail(string emailAddress)
        {
            var user = await _userManager.FindByEmailAsync(emailAddress);
            if (user == null)
            {
                return Ok();
            }

            return Ok();
        }

        /// <summary>
        /// This method logs the user out. It uses the SignInManager which will ensure that the httpsession is destroyed correctly.
        /// </summary>
        /// <returns>An empty HttpResult</returns>
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();           
            return Ok();
        }

        /// <summary>
        /// This method retrieves the added external login providers added in Startup.cs and creates a list of Signinproviders for the client to display.
        /// </summary>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> LoginProviders()
        {
            return Ok((await _signInManager.GetExternalAuthenticationSchemesAsync())
                  .Select(s => new SigninProvider()
                  {
                      Name = s.Name,
                      DisplayName = s.DisplayName
                  }));
        }

        /// <summary>
        /// T
        /// </summary>
        /// <param name="provider">Name of external login provider</param>
        /// <returns>A ChallengeResult</returns>
        [HttpPost, AllowAnonymous]
        public async Task<IActionResult> ExternalLogin([FromForm] string provider)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = UrlHelperExtensions.Action(this.Url, "ExternalLoginCallback", "Authorize");
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        /// <summary>
        /// This method will attempt to login the user with the external login information.
        /// If the user does not exist in the database it will create a new user based on the information pulled from the external login provider.
        /// </summary>
        /// <param name="returnUrl">The url to redirect on success.</param>
        /// <param name="remoteError">Error received from the external login provider.</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = "/dashboard", string remoteError = null)
        {
            if (remoteError != null)
            {
                return RedirectLoginFailed();
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return null;
            }

            var signinResult = await LoginExternalUser(info);
            if (signinResult == null)
            {
                return RedirectLoginFailed();
            }
            else if (signinResult.Succeeded)
            {
                return RedirectLoginSuccess();
            }

            var user = new ApplicationUser();
            user.UserData = new UserData();

            
            if (info.Principal.Identity.Name != null)
            {
                var userName = info.Principal.Identity.Name.Replace(" ", "").Replace("#", "");
                user.UserName = await CreateUniqueUsername(userName);
            }

            var claims = info.Principal.Identities.First().Claims;
            var filteredclaims = claims.Where(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").ToList();
            if (filteredclaims.Any())
            {
                user.Email = filteredclaims.Select(x => x.Value).ToList().First();
                user.EmailConfirmed = true;
            }

            IActionResult returnval = await ExternalRegister(user);
            return returnval ?? Redirect(returnUrl);
        }

        /// <summary>
        /// Method to create an external user based on information received from the external login provider.
        /// </summary>
        /// <param name="user">The user information received from the external login provider.</param>
        /// <returns>IActionResult that redirect the user to either the /login or authorized landing page.</returns>
        public async Task<IActionResult> ExternalRegister(ApplicationUser user)
        {
            var info = await _signInManager.GetExternalLoginInfoAsync();
            var result = await _userManager.CreateAsync(user);

            if (!result.Succeeded)
            {
                return BadRequest(result.Errors.FirstOrDefault()?.Description);
            }

            if (user.Id != null)
            {
                var userLoginResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(info.LoginProvider, info.ProviderKey, info.LoginProvider));
                if ( userLoginResult == null || !userLoginResult.Succeeded )
                {
                    return RedirectLoginFailed();
                }

                var signinResult = await LoginExternalUser(info);
                if (signinResult != null && signinResult.Succeeded)
                {
                    return RedirectLoginSuccess();
                }
            }

            return RedirectLoginFailed();
        }
        /// <summary>
        /// Log in external user.
        /// </summary>
        /// <param name="info">external login information for the user.</param>
        /// <returns>An identity SignInResult</returns>
        private async Task<Microsoft.AspNetCore.Identity.SignInResult> LoginExternalUser(ExternalLoginInfo info)
        {
            var signinResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            return signinResult;
        }

        /// <summary>
        /// private method to redirect to /login
        /// </summary>
        /// <returns></returns>
        private IActionResult RedirectLoginFailed()
        {
            return Redirect("/login");
        }

        /// <summary>
        /// private method to redirect to /dashboard
        /// </summary>
        /// <returns></returns>
        private IActionResult RedirectLoginSuccess()
        {
            return Redirect("/dashboard");
        }

        /// <summary>
        /// Generates a unique username from provided username
        /// </summary>
        /// <param name="username">Username to uniqify</param>
        /// <returns>uniqified username.</returns>
        private async Task<string> CreateUniqueUsername(string username)
        {
            username = username.Replace(" ", "").Replace("#", "");
            var count = 0;
            var random = new Random((int)DateTime.Now.Ticks);
            for (int i = 0; i < 1000; i++)
            {
                var existingUser = await _userManager.FindByNameAsync(username + "#" + (count = random.Next(1001, 9999)));
                if (existingUser == null)
                {
                    username = username + "#" + count;
                    break;
                }
            }
            return username;
        }
    }
}
