﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorSecureLogin.Server.Data;
using BlazorSecureLogin.Server.Models;
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Shared;
using BlazorSecureLogin.Shared.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceStack.OrmLite;

namespace BlazorSecureLogin.Server.Controllers
{
    /// <summary>
    /// Custom controller that provides API functionality for the user.
    /// </summary>
    [ApiController, Route("api/[controller]/[action]")]
    public class UserController : MyControllerBase
    {
        private IUserHelper _UserHelper { get; set; }
        public UserController(IUserHelper userHelper)
        {
            this._UserHelper = userHelper;
        }

        /// <summary>
        /// Database query to fetch the information on the user in the local field CurrentUser.
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public async Task<IActionResult> GetInfo()
        {
            string aspNetUserName = "";
            UserData currentUser = new UserData();
            OrmLiteHelper.OpenDbConnection(Db => {
                aspNetUserName = this.GetAspNetUserNameById(_UserHelper.getCurrentUserId());
                currentUser = Db.Select(Db.From<UserData>().Where(x => x.UserId == _UserHelper.getCurrentUserId())).FirstOrDefault();
            });
            return Ok(BuildInfo(currentUser, aspNetUserName));
        }

        /// <summary>
        /// Method to translate from the serverside ApplicationUser object to the shared UserInfo object.
        /// </summary>
        /// <param name="user">The user object to generate the UserInfo from</param>
        /// <returns>
        /// A shared UserInfo object that is safe to send, as it does not contain security information.
        /// </returns>
        public UserInfo BuildInfo(UserData user, string aspNetUserName)
        {
            if (user == null)
            {
                return null;
            }

            return new UserInfo()
            {
                OriginalUserName = aspNetUserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                BirthDate = user.BirthDate,
                Gender = user.Gender,
                Country = user.Country,
                PostalCode = user.PostalCode,
                Address1 = user.Address1,
                Address2 = user.Address2
            };
        }

        /// <summary>
        /// API endpoint for the user model used to change the user information for the CurrentUser.
        /// </summary>
        /// <param name="info">Shared UserInfo object</param>
        /// <returns>Http result having a updated UserInfo object nested in the body.</returns>
        [HttpPost, Authorize]
        public async Task<IActionResult> PostInfo(UserInfo info)
        {
            string aspNetUserName = "";
            var newUserData = new UserData();
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                var checkQuery = Db.From<UserData>().Where(x => x.UserId == _UserHelper.getCurrentUserId());
                if (!Db.Exists(checkQuery))
                {
                    throw new Exception("User does not exist in database");
                }

                var userData = new UserData();
                userData.FirstName = info.FirstName;
                userData.LastName = info.LastName;
                if (info.BirthDate.HasValue)
                {
                    userData.BirthDate = info.BirthDate.Value.ToLocalTime().Date;
                }

                userData.Gender = info.Gender;
                userData.Country = info.Country;
                userData.PostalCode = info.PostalCode;
                userData.Address1 = info.Address1;
                userData.Address2 = info.Address2;
                userData.UserId = _UserHelper.getCurrentUserId();
                Db.Update(userData);

                newUserData = Db.Select(Db.From<UserData>().Where(x => x.UserId == userData.UserId)).FirstOrDefault();
                aspNetUserName = this.GetAspNetUserNameById(userData.UserId);
            });
            
            return Ok(BuildInfo(newUserData, aspNetUserName));
        }

        /// <summary>
        /// API endpoint to get the current users connected organizations
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetUserOrganizations()
        {
            return Ok(this._UserHelper.GetUserOrganizations());
        }

        /// <summary>
        /// API endpoint to get current users connected entities
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetUserEntities()
        {
            return Ok(this._UserHelper.GetUserEntities());
        }

        #region Private helper methods
        public string GetAspNetUserNameById(Guid userId)
        {
            string output = null;
            OrmLiteHelper.OpenDbConnection(Db =>
            {
                output = Db.SqlScalar<string>("SELECT [UserName] FROM [dbo].[AspNetUsers] [anu] WHERE [anu].[Id] = @UserId", new { UserId = userId });
            });

            return output;
        }

        #endregion
    }
}