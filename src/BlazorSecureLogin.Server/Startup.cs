/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/startup?view=aspnetcore-2.2
/// File description: This file contains configuration options for the application.
using BlazorSecureLogin.Server.Data;
using BlazorSecureLogin.Server.Models;
using BlazorSecureLogin.Server.Services.Contracts;
using BlazorSecureLogin.Server.Services.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Server;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceStack;
using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server
{
    public class Startup
    {
        private readonly IConfiguration Configuration;
        public Startup (IConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Registers ServiceStack.OrmLite
            Licensing.RegisterLicense("TRIAL30MNIELSON-e1JlZjpUUklBTDMwTU5JRUxTT04sTmFtZTpNYXJ0aW4gTmllbHNlbixUeXBlOlRyaWFsLE1ldGE6MCxIYXNoOklVaHo4UWJjQk5Wa0tLbzNiY1hpb3haVVdyQ0syckRadnI0TVNFa0lIUGFrSkM3cXUyYnJHUVVyQ1BMcUdaOHRuRk9kcUNkbm9IU2NLUVJMUERlS2p6MDJLdXJHcW5OSkZrV1pqSXhtL2lmdU43ZE1TZFNpb3o0RXBMeHpnbXVQaHo3a2cwOFNtQ01LTDc2Nlc5aDM1dFAwV1ArbFpEYXNpN0hkekdXdWV4Zz0sRXhwaXJ5OjIwMTktMTItMTl9");
            // Creates the database context and uses the connection string specified in appsettings.json / appsettings.development.json depending on deployment type.
            services.AddDbContext<ApplicationDbContext>(options =>
              options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            // Adds Identity and Entity Framework
            services.AddIdentity<ApplicationUser, IdentityRole<Guid>>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Adds basic authentication with Entity Framework as well as all the added external login providers
            // *Note: If the project does not contain the User Secrets corrensponding to the login providers, the project will fail on startup.
            // *Note: Microsoft.AspNetCore.Authentication.<Login provider> package must be included in the project.
            services.AddAuthentication()
                .AddFacebook(facebookOptions =>
                {
                    facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                    facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                })
                .AddGoogle(googleOptions =>
                {
                    googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                    googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                })
                .AddMicrosoftAccount(microsoftOptions => 
                {
                    microsoftOptions.ClientId = Configuration["Authentication:MicrosoftAccount:ClientId"];
                    microsoftOptions.ClientSecret = Configuration["Authentication:MicrosoftAccount:ClientSecret"];
                })
                .AddTwitter(twitterOptions => 
                {
                    twitterOptions.ConsumerKey = Configuration["Authentication:Twitter:ConsumerAPIKey"];
                    twitterOptions.ConsumerSecret = Configuration["Authentication:Twitter:ConsumerSecret"];
                });


            // Configures set of optoins for Microsoft.Identity
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                
                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;
                // User settings
                options.User.RequireUniqueEmail = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+#";
                //options.SignIn.RequireConfirmedEmail = true;
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = false;
                options.Events.OnRedirectToLogin = context =>
                {
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                };
            });

            // Adds MVC services
            services.AddMvc();
            services.AddControllers().AddNewtonsoftJson();
            // Adds a default implementation of ASPNET.CORE httpContextAccessor to allow us to access the http context (client session)
            services.AddHttpContextAccessor();
            // Dependency injection 
            services.AddScoped<IUserHelper, UserHelper>();
            services.AddScoped<BaseHelper>();
            services.AddScoped<IOrganizationHelper, OrganizationHelper>();
            services.AddScoped<IGroupHelper, GroupHelper>();
            services.AddScoped<IEntityHelper, EntityHelper>();
            services.AddScoped<ICalendarHelper, CalendarHelper>();

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    MediaTypeNames.Application.Octet
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                // Creates missing tables if the migration row on the database does not match the latest EF migration
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
            }

            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBlazorDebugging();
            }

            app.UseClientSideBlazorFiles<Client.Startup>();
            
            // enables mvc routing, authentication and authorization
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            // Maps that default page is index.html
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToClientSideBlazor<Client.Startup>("index.html");
            });
        }
    }
}
