﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
/// File description: This file contains the model for the User information.

using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace BlazorSecureLogin.Server.Models
{

    /// <summary>
    /// Model to store User information in.
    /// </summary>
    [Alias("UserDatas")]
    public class UserData
    {
        [Key] public Guid UserId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Calculate a given users current age from this.Birthdate.
        /// This property has the NotMapped attributes which makes Entity Framework ignore it.
        /// </summary>
        [NotMapped, Ignore]
        public int? Age
        {
            get
            {
                if (this.BirthDate == null)
                {
                    return null;
                }

                var today = DateTime.Today;
                var age = today.Year - this.BirthDate.Value.Year;
                if (this.BirthDate.Value.Date > today.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
        }
        public int? Gender { get; set; }
        public String Country { get; set; }
        public String PostalCode { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        /// <summary>
        /// Add foreign key reference to ApplicationUser
        /// </summary>
        [Ignore]
        public ApplicationUser User { get; set; }
    }
}