﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// External credits: Anders Kehlet
/// File description: Default identity framework class for AspNetUsers.
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Models
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        /// <summary>
        /// Foreign key reference from subtable UserDatas to ApplicationUser
        /// </summary>
        public UserData UserData { get; set; }
    }
}
