﻿/// TEC - Tecnical Education Center
/// Blazor Secure Login
/// Author: Microsoft ASP.NET Identity Framework
/// Editor: Steffen Klinge
/// External credits: Anders Kehlet
/// File description: This file contains the application database modifications from the original identity database structure.
/// This class must contain all the custom database tables that Entity Framework should support.
using BlazorSecureLogin.Server.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        /// <summary>
        /// Creates a table using the structure from the model/UserData class.
        /// </summary>
        public DbSet<UserData> UserDatas { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public ApplicationDbContext() {}

        /// <summary>
        /// Basic configuration of the Database context
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// In this method you can specify set of rules that EF takes in account when creating the models in the database.
        /// e.g. You can omit one or more properties from each model or rename the standard ASP.NET identity table names
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Google: Entityframework core fluid api
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>();
        }

    }
}
