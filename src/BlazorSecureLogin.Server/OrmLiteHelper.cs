﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Server
{
    /// <summary>
    /// Helper class to initialize database connections and execute database operations such as CREATE, READ, UPDATE, DELETE and execute Stored Procedures
    /// </summary>
    public static class OrmLiteHelper
    {
        private static readonly object factoryLock = new object();
        private static IDbConnectionFactory factory;
        private static IDbConnectionFactory DbConnectionFactory
        {
            get
            {
                // Object lock, to ensure that multiple unnecessary connections to the database are not created.
                lock (factoryLock)
                {
                    // Lazy load implementation
                    if (factory == null)
                    {
                        // assigns the factory variable with a new connection factory
                        factory = CreateConnectionFactory("Server=localhost\\SQLEXPRESS;Database=PlanDB;Trusted_Connection=True;");
                    }
                    return factory;
                }
            }
        }

        private static IDbConnectionFactory CreateConnectionFactory(string connectionString)
        {
            var build = new SqlConnectionStringBuilder(connectionString);
            // Defines the connect timeout in seconds. If the connection to the database has not been made in 120 seconds, it will cancel the request. This is specified to avoid waiting for a closed database forever.
            build.ConnectTimeout = 30;
            // Force english language so dates are formatted as expected
            build.CurrentLanguage = "English";

            // Create the actual connection to the database
            var factory = new OrmLiteConnectionFactory(build.ConnectionString, SqlServerOrmLiteDialectProvider.Instance, true);
            // Enable automatically disposal of the connection when it is no longer used. If this is not specified, the app will use the same connection to the database throughout the lifetime of the app, which can lead to problems
            factory.AutoDisposeConnection = true;

            // Subscribes another action to the OnDispose event that will make sure to close open connections when the factory is disposed.
            factory.OnDispose += (connection) =>
            {
                if (connection != null && connection.State == ConnectionState.Open)
                    connection.Close();
            };
            // returns the factory ready to be used
            return factory;
        }

        /// <summary>
        /// Executes the action in the database context.
        /// Following example shows how to use it to delete records from persons with the name "Henrik" 
        /// OrmLiteHelper.OpenDbConnection(Db => { Db.Delete<Person>().Where(x => x.Name == "Henrik")});
        /// </summary>
        /// <param name="action">The database operation</param>
        public static void OpenDbConnection(Action<IDbConnection> action)
        {
            try
            {
                // Gets the DbConnectionFactory and calls its inherited method OpenDbConnection.
                using (var connection = DbConnectionFactory.OpenDbConnection())
                {
                    // Executes the action and passes the opened database connection.
                    action(connection);
                }
            }
            catch (SqlException e)
            {
                // Raises any exception of type SqlExpression
                throw e;
            }
        }
    }
}
