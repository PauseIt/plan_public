﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    public enum ReferenceType
    {
        Organization,
        Group,
        Entity
    }

    public enum DialogAction
    {
        Create,
        Update,
        Delete
    }

    public enum PayloadStatus
    {
        Source,
        Target
    }
    
    public enum CalendarType
    {
        Organization,
        Group,
        Entity,
        User
    }
}
