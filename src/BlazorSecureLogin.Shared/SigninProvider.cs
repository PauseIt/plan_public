﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold available singin providers
    /// </summary>
    public class SigninProvider : BaseModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}
