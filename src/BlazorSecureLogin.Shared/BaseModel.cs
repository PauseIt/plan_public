﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// Base class that most DTO's implement.
    /// </summary>
    public class BaseModel : ICloneable
    {
        /// <summary>
        /// Not used anymore
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public virtual StringContent ToJson()
        {
            return new StringContent(JsonConvert.SerializeObject(this), Encoding.UTF8, "application/json");
        }

        /// <summary>
        /// This is the implentation of ICloneable and will clone the object (without object reference).
        /// </summary>
        /// <returns>Clone of this.</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
