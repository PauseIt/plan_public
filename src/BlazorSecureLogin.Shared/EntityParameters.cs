﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System.ComponentModel.DataAnnotations;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold the information about a entity for API endpoints.
    /// </summary>
    public class EntityParameters : BaseModel
    {
        public EntityParameters()
        {
            this.Entity = new Entity();
        }

        public EntityParameters(Entity entity)
        {
            this.Entity = entity;
        }

        public EntityParameters(EntityParameters entityParameters)
        {
            this.Entity = entityParameters.Entity;
        }

        [Required]
        public Entity Entity { get; set; }

        /// <summary>
        /// Method to check if the object is valid.
        /// * NOT IMPLEMENTED
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return true;
        }
    }
}
