﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO used for user invitaion api endpoints
    /// </summary>
    public class UserInviteParameters
    {
        public ReferenceType ReferenceType { get; set; }
        public long ReferenceId { get; set; }
        public string Email { get; set; }
    }
}
