﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold organization information for API endpoints
    /// </summary>
    public class OrganizationParameters : BaseModel
    {
        public OrganizationParameters()
        {
            this.Organization = new Organization();
        }

        public OrganizationParameters(Organization organization)
        {
            this.Organization = organization;
        }

        public OrganizationParameters(OrganizationParameters organizationParameters)
        {
            this.Organization = (Organization)organizationParameters.Organization.Clone();
        }

        [Required]
        public Organization Organization { get; set; }

        /// <summary>
        /// This method will check if the object is valid before working with it
        /// * NOT IMPLEMENTED
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return true;
        }
    }
}
