﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold the GET parameters for Calendar get requests.
    /// </summary>
    public class CalendarParameters
    {
        public CalendarType CalendarType { get ; set; }
        public long Id { get; set; }
    }
}
