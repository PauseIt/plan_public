﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold information about group for API endpoints
    /// </summary>
    public class GroupParameters : BaseModel
    {
        public GroupParameters()
        {
            this.Group = new Group();
        }

        public GroupParameters(Group group)
        {
            this.Group = group;
        }

        [Required]
        public Group Group { get; set; }

        /// <summary>
        /// This method will check if the object is valid before working with it
        /// * NOT IMPLEMENTED
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return true;
        }
    }
}
