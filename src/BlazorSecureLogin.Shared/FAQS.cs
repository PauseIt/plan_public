﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO To hold the FAQ information for API endpoints.
    /// </summary>
    public class FAQS
    {
        public string Subject { get; set; }
        public ICollection<Answers> Answers { get; set; }
    }
}