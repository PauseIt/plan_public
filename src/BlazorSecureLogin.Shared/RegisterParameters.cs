﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: Microsoft ASP.NET Identity best pratice
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to hold user information for users that want to register
    /// a new user to the system.
    /// </summary>
    public class RegisterParameters : BaseModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare(nameof(Password), ErrorMessage = "Passwords do not match")]
        public string PasswordConfirm { get; set; }

        public string Email { get; set; }
    }
}
