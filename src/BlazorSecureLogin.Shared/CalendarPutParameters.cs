﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    public class CalendarPutParameters
    {
        /// <summary>
        /// DTO to hold the information about a calendar event created
        /// to send to server API endpoint to be saved.
        /// </summary>
        public CalendarPutParameters()
        {
            this.CalendarData = new CalendarData();
            this.Ids = new List<long>();
        }

        public CalendarData CalendarData { get; set; }

        // put into list 
        private CalendarType? _CalendarType { get; set; }
        public CalendarType? CalendarType 
        {
            get
            {
                return this._CalendarType;
            }
            set 
            { 
                if (value == null || value.Value == BlazorSecureLogin.Shared.CalendarType.User) {
                    this._CalendarType = null;
                    return;
                }

                this._CalendarType = value;
            } 
        }

        public List<long> Ids { get; set; }
        public bool AdminCalendar { get; set; }
        public bool AllGroups { get; set; }
    }
}
