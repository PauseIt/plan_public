﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO to store user data
    /// </summary>
    public class UserInfo : BaseModel
    {
        public string UserName
        { 
            get {
                if (String.IsNullOrEmpty(this.OriginalUserName))
                {
                    return null;
                }
                var index = this.OriginalUserName.LastIndexOf("#");
                return this.OriginalUserName.Substring(0, index);
            }
        }
        public string OriginalUserName { get; set; }
        public string NewUserName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? Age
        {
            get
            {
                if (this.BirthDate == null)
                {
                    return null;
                }

                var today = DateTime.Today;
                var age = today.Year - this.BirthDate.Value.Year;
                if (this.BirthDate.Value.Date > today.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
        }
        public int? Gender { get; set; }
        public String Country { get; set; }
        public String PostalCode { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
    }
}
