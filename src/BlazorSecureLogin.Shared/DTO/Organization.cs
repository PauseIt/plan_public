﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.DataAnnotations;
using System;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table Organization
    /// </summary>
    public class Organization : BaseModel
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }
        [System.ComponentModel.DataAnnotations.Required]
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
