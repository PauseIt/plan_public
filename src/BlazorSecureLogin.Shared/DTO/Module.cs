﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table Module
    /// </summary>
    public class Module : BaseModel
    {
        [PrimaryKey, AutoIncrement] 
        public long Id { get; set; }
        public string Title { get; set; }
    }
}
