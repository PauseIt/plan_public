﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table User_Entity
    /// </summary>
    public class User_Entity : BaseModel
    {
        public Guid AspNetUsers_Id { get; set; }
        public long Entity_Id { get; set; }
    }
}
