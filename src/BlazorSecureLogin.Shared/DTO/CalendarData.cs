﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table CalendarData
    /// </summary>
    public class CalendarData
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }
        [Ignore]
        public string Subject
        {
            get
            {
                return Title + Description;
            }
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public long SourceOrganization_Id { get; set; }
        public long? SourceGroup_Id { get; set; }
    }
}
