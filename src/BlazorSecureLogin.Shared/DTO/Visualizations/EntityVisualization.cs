﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO.Visualizations
{
    /// <summary>
    /// DTO to hold entity information and information about its administrator, linked groups and creator
    /// </summary>
    public class EntityVisualization
    {
        public Entity Entity { get; set; }
        public List<string> LinkedGroups { get; set; }
        public List<string> Owners { get; set; }
        public string CreatedByName { get; set; }
    }
}
