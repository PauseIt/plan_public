﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO.Visualizations
{
    /// <summary>
    /// DTO to hold group information and information about its administrator and creator
    /// </summary>
    public class GroupVisualization
    {
        public Group Group { get; set; }
        public List<string> Administrators { get; set; }
        public string CreatedByName { get; set; }
    }
}
