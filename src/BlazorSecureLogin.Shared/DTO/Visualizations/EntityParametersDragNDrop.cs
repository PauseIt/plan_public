﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using BlazorSecureLogin.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSecureLogin.Client.Pages.Layouts
{
    /// <summary>
    /// Visualization DTO that is used when adding entities to new groups with the drag n drop functionality
    /// </summary>
    public class EntityParametersDragNDrop : EntityParameters
    {
        public EntityParametersDragNDrop(EntityParameters entityParameters) : base(entityParameters) { }
        public EntityParametersDragNDrop() : base() { }
        public PayloadStatus Status { get; set; }
        public bool InTargetGroup { get; set; }
        public long NewGroupId { get; set; }
    }
}
