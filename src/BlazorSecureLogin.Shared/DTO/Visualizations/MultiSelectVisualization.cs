﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO.Visualizations
{
    /// <summary>
    /// DTO to hold data for a multiselect element
    /// </summary>
    public class MultiSelectVisualization
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
}
