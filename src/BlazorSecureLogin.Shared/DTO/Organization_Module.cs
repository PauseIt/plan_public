﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table Organization_Module
    /// </summary>
    public class Organization_Module : BaseModel
    {
        public long Organization_Id { get; set; }
        public long Module_Id { get; set; }
    }
}
