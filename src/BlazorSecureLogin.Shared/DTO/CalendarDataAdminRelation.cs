﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared
{
    /// <summary>
    /// DTO for database table CalendarDataAdminRelation
    /// </summary>
    public class CalendarDataAdminRelation
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }
        public long CalendarData_Id { get; set; }
        public long? Group_Id { get; set; }
        public long? Organization_Id { get; set; }
        public bool AllGroups { get; set; }
    }
}
