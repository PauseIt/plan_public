﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table User_Group
    /// </summary>
    public class User_Group : BaseModel
    {
        public Guid AspNetUsers_Id { get; set; }
        public long Group_Id { get; set; }
    }
}
