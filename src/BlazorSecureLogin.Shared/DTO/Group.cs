﻿/// TEC - Tecnical Education Center
/// PLAN - Organizing platform
/// Author: Martin Puge & Steffen Klinge
/// Editor: N/A
/// External credits: N/A
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorSecureLogin.Shared.DTO
{
    /// <summary>
    /// DTO for database table Organization_Module
    /// </summary>
    public class Group : BaseModel, ICloneable
    {
        [PrimaryKey, AutoIncrement] 
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long Organization_Id { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
